#include "brute_solver.h"
#include <filesystem>
#include <fstream>
#include <stack>
#include <iostream>
namespace fs = std::filesystem;

BruteSolver::BruteSolver(const char* test_path_, const char* sol_path_) : tests_path(test_path_),
  solution_path(sol_path_){};

void BruteSolver::solveAll() {

  for(auto& p : fs::directory_iterator(tests_path)){
    std::cout << p << std::endl;
    solve(p.path());
  }

}

std::unique_ptr< Graph > BruteSolver::decode(const std::string& path){
  std::fstream s(path, s.binary | s.in  );
  if(!s.is_open()){
    std::cerr << "File " << path << " could not be open!" << std::endl;
    return NULL;
  }
  
  long long totalSum = 0;
  int v, e, v1, v2, w;
  s >> v >> e;
  
  auto G = std::make_unique< Graph > (v, e);
//  std::cerr << "file: " << path << " v: " << v << " e: " << e << std::endl;

  for(int i = 0; i < e; ++i){
    s >> v1 >> v2 >> w;
//    std::cerr << "edge: " << v1 << " " << v2 << " w: " << w << std::endl;
    totalSum += w;
    (*(G->G))[v1].push_back(Edge(v2,w));
    (*(G->G))[v2].push_back(Edge(v1,w));
  }
  
  std::cerr << "totalSum: " << totalSum << std::endl;
  return G;
}

long long BruteSolver::addEdges(int vertex, unsigned long long mask, std::unique_ptr<Graph>& G){
  long long sum = 0;
  for(Edge& e : (*(G->G))[vertex]){
    if(!( mask & (1 << (e.v-1))) ){
      sum += e.w;
    }
  }
  return sum;
}

void BruteSolver::saveData(long long best_cut, unsigned long long best_mask, std::unique_ptr<Graph>& G, std::string& testName){
  std::cerr << "Test: " << testName << std::endl;
  std::cerr << "best_cut: " << best_cut;
  std::stack<int> S;
  for(int i = 0; i < G->v; ++i){
    S.push( bool(( 1 << i) & best_mask) );
  }
  std::cerr << " mask: ";
  while(!S.empty()){
    std::cerr << S.top() << " ";
    S.pop();
  }
}

double BruteSolver::solve(const std::string& path) {
  auto G = decode(path);

  unsigned long long MAX = (1 << G->v);

  long long best_cut = -1;
  unsigned long long best_mask;
  std::cerr << "max is: " << MAX << std::endl;
  for(unsigned long long mask = 0; mask < MAX; ++mask){
    long long cut = 0;
    if(mask % 1000000 == 0) std::cerr << "mask: " << mask << std::endl;
    for(unsigned long long i = 0; i < G->v; ++i){
      if( (1<<(i)) & mask ) {
        cut += addEdges(i+1, mask, G);
      }
    }
    if(cut > best_cut){
      best_cut = cut;
      best_mask = mask;
    }
  }
  
  auto pos = path.find_last_of('/');
  std::string testName = path.substr(pos+1);
  saveData(best_cut, best_mask, G, testName);
  
  return (double)best_cut;
}
