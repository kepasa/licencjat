#include <iostream>
#include <sstream>
#include "commands.h"
#include "tig.h"
#include "brute_solver.h"
#include "sdp_solver.h"

static const char* TEST_SAVE_PATH = "./test/small_random";
static const char* SOLUTION_SAVE_PATH = "./solution";

void run(CommandManager& cmdManager) {
  std::string lines, command;
  while(true){
    std::getline(std::cin, lines);
    std::stringstream line(lines);
    
    cmdManager.getAndExecuteCommand(line);
  }
}

int main(){
  std::cout << "Max Cut - approx. algorithm using SDP" << std::endl;
  TestInstanceGenerator tig(TEST_SAVE_PATH);
  BruteSolver bSolver(TEST_SAVE_PATH, SOLUTION_SAVE_PATH);
  SdpSolver sdpSolver(TEST_SAVE_PATH, SOLUTION_SAVE_PATH);
  CommandManager cmdManager(tig, bSolver, sdpSolver, SOLUTION_SAVE_PATH);
  
  run(cmdManager);
  
  return 0;
}
