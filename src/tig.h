#ifndef SDP_TEST_INSTANCE_GENERATOR
#define SDP_TEST_INSTANCE_GENERATOR
#include <memory>
#include <vector>
#include <tuple>
#include "graph.h"

class TestInstanceGenerator {
  public:
    TestInstanceGenerator(const char* save_path_);
    void generate(int v, int e, int minW, int maxW);
    void dumpData(Graph& g);
    private:
    const char* save_path;
    static int G_COUNTER;
};


#endif
