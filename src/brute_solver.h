#ifndef SDP_BRUTE_SOLVER
#define SDP_BRUTE_SOLVER
#include <string>
#include <memory>
#include <vector>
#include <tuple>
#include "graph.h"
class BruteSolver{
  public:
    BruteSolver(const char* test_path_, const char* sol_path_);
    void solveAll();
    double solve(const std::string& filename);
    std::unique_ptr< Graph > decode(const std::string& path);

  private:
    long long addEdges(int vertex, unsigned long long mask, std::unique_ptr<Graph>& G);
    void saveData(long long best_cut, unsigned long long best_mask, std::unique_ptr<Graph>& G, std::string& testName);
    const char* tests_path;
    const char* solution_path;
};


#endif
