#ifndef SDP_GRAPH_HEADER
#define SDP_GRAPH_HEADER
#include <vector>
#include <array>

struct Edge {
  int v;
  int w;
  Edge(int v_, int w_) : v(v_), w(w_) {};
};

struct Graph{
  int v, e;
  std::unique_ptr< std::vector<std::vector<Edge>> > G;
  Graph(int v_, int e_) : v(v_), e(e_) {
    G = std::make_unique<std::vector<std::vector<Edge>>>();
    G->resize( v+1 );
  };
  Graph(int v_, int e_, std::unique_ptr< std::vector<std::vector<Edge>>>& G_) : v(v_), e(e_), G(std::move(G_)) {};
};

#endif
