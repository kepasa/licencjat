#ifndef SDP_SOLVER_HEADER
#define SDP_SOLVER_HEADER
#define dbg if(0)
#include <string>
#include <memory>
#include <vector>
#include <tuple>
#include <cmath>
#include "graph.h"
#include "solution.h"
#include <iostream>
#include <fstream>
#include <random>
extern "C" {
#include "../include/declarations.h"
}


class SdpSolver {
  public:
    SdpSolver(const char* test_path_, const char* sol_path_);
    Solution solve(const std::string& filename);
    std::unique_ptr<Graph> decode(const std::string& path);
    Solution randomizedRounding(blockmatrix X, std::unique_ptr< Graph>& G);
    void choleskyDecomposition(double** M, double** V, int v);

  private:
    const char* test_path;
    const char* solution_path;
    double alpha;
    double eps;
};


#endif
