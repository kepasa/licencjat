#include "tig.h"
#include "graph.h"
#include <random>
#include <chrono>
#include <functional>
#include <tuple>
#include <set>
#include <memory>
#include <iostream>
#include <fstream>

TestInstanceGenerator::TestInstanceGenerator(const char* save_path_) : save_path(save_path_) {};

int TestInstanceGenerator::G_COUNTER = 1;

void TestInstanceGenerator::generate(int v, int e, int minW, int maxW){
  auto edgeSet = std::make_unique< std::set< std::pair<int, int> >>();
  auto weightedEdgeSet = std::make_unique<std::vector<std::vector< Edge >>>();
  auto seed_v = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  auto seed_w = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  
  auto vertex_rand = std::bind(std::uniform_int_distribution<int>(1, v), std::mt19937(seed_v));
  auto weight_rand = std::bind(std::uniform_int_distribution<int>(minW, maxW), std::mt19937(seed_w));
  
  int v1, v2, w;
  
  for(int i = 0; i < e;){ // first generate edges
    v1 = vertex_rand();
    do {
      v2 = vertex_rand(); 
    }while(v1 == v2);
    if(v1 > v2) std::swap(v1, v2);
    
    //std::cerr << "Generated pair: " << v1 << " " << v2 << std::endl;

    if( edgeSet->find(std::make_pair(v1, v2)) == edgeSet->end()){
      //std::cerr << "Inserted pair: " << v1 << " " << v2 << std::endl;
      edgeSet->insert(std::make_pair(v1, v2));
      ++i;
    }
  }
  weightedEdgeSet->resize(v+1);
  for(const std::pair<int,int>& p : *edgeSet){
    w = weight_rand();
    (*weightedEdgeSet)[p.first].push_back(Edge(p.second, w) );
    (*weightedEdgeSet)[p.second].push_back(Edge(p.first, w) );
  }
  Graph g(v, e, weightedEdgeSet);
  this->dumpData(g);
}

void TestInstanceGenerator::dumpData(Graph& g){
  std::string path(this->save_path);
  path.append("//G" + std::to_string(G_COUNTER));
  std::fstream s(path, s.binary | s.trunc | s.out);
  s << g.v << " " << g.e << '\n';
  if(!s.is_open()){
    std::cerr << "Failed to open file!" << std::endl;
  }else
    std::cerr << "Created file" << " " << path << std::endl;

  for(int i = 1; i <= g.v; ++i){
    for(Edge& E : (*(g.G))[i]) {
      if((*g.G)[i].size() == 0) continue;
      if(i > E.v) continue;
      s << i << " " <<
      E.v << " " << E.w << '\n';
    }
  }
  this->G_COUNTER++;
  s.close();
}
