#include "commands.h"
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <filesystem>
#include "solution.h"

CommandManager::CommandManager(TestInstanceGenerator& tig_, BruteSolver& bSolver_, SdpSolver& sdpSolver_, const char* solution_path_) : tig(tig_), bSolver(bSolver_), sdpSolver(sdpSolver_), solution_path(solution_path_) {
  initNameSolutionMap();
};

void CommandManager::initNameSolutionMap(){
  std::string save = std::string(this->solution_path);
  save.append("/");
  save.append("maxcut_bestvalues.txt");
  
  std::ifstream file;
  file.open(save);
  std::string key, val;
  while(file >> key >> val){
    this->graphNameToSolution.insert({key, val});
  }
}

void CommandManager::getAndExecuteCommand(std::stringstream& line){
  std::string command;
  line >> command;
  //std::cerr << "got command: " << command << std::endl;

  CommandMap::iterator funct = this->cmdMap.find(command);
  if( funct != this->cmdMap.end() ){
    (this->*(funct->second))(line);
  } else {
    std::cout << "Command '" << command << "' is not supported." << std::endl;
  }
}

void CommandManager::executeExit(std::stringstream& line){
  int code;
  line >> code;
  std::cout << "Exiting with code " << code << std::endl;
  std::exit(code);
}

void CommandManager::executeGenerate(std::stringstream& line) {
  int n, v, e, minW, maxW;
  line >> n >> v >> e >> minW >> maxW;

  for(int i = 0; i < n; ++i){
    tig.generate(v, e, minW, maxW);
    std::cerr << "Generated graph: " << i << std::endl; 
  }
}

void CommandManager::executeSolveAll(std::stringstream& line){
  this->bSolver.solveAll();
}

void CommandManager::testSdp(std::stringstream& line){
  std::string path;
  line >> path;
  namespace fs = std::filesystem;
  
  std::string save = std::string(this->solution_path);
  save.append("/");
  save.append("randomTests");
  
  std::ofstream file;
  file.open(save);
  int counter = 0;
  double avgApproxRatio = 0.0;

  for(auto& p : fs::directory_iterator(path)){
    std::cout << p << std::endl;

    Solution sdpSol = this->sdpSolver.solve(p.path());
    double bruteSol = this->bSolver.solve(p.path());
    
    std::cerr << p.path() << " sdp: " << sdpSol.sdpVal << " brute: " << bruteSol << " round: " << sdpSol.val << std::endl;
    
    const std::string& testPath = p.path();
    auto pos = testPath.find_last_of('/');
    std::string testName = testPath.substr(pos+1);
    
    file << "Graph: " << testName << " approx: " << sdpSol.val << " exact: " << bruteSol << std::endl;
    avgApproxRatio  += sdpSol.val / bruteSol;
    ++counter;

    if(0.87856*sdpSol.sdpVal < bruteSol && bruteSol <= sdpSol.sdpVal && sdpSol.val >= 0.88*bruteSol){
      std::cerr << "OK!" << std::endl;
    }else{
      std::cerr << "FAIL!" << std::endl;
      exit(1);
    }
  }
  file << "Average approximation ratio is: " << (avgApproxRatio/(double)counter) << std::endl;
  
}
  
void CommandManager::solveSdp(std::stringstream& line){
  std::string path;
  line >> path;
  namespace fs = std::filesystem;
  
  std::string save = std::string(this->solution_path);
  save.append("/");
  save.append("bigTests");
  
  std::ofstream file;
  file.open(save);
  int counter = 0;
  double avgApproxRatio = 0.0;

  for(auto& p : fs::directory_iterator(path)){
    std::cout << p << std::endl;
    
    Solution sdpSol = this->sdpSolver.solve(p.path());
    const std::string& testPath = p.path();
    auto pos = testPath.find_last_of('/');
    std::string testName = testPath.substr(pos+1);
    auto pos2 = testName.find_last_of('.');
    testName = testName.substr(0, pos2);
    std::string tmpstr1 = this->graphNameToSolution[testName];
    double result = std::stod(this->graphNameToSolution[testName]);
    
    std::cerr << testName << " sdp: " << sdpSol.sdpVal << " round: " << sdpSol.val << " result: " << tmpstr1 << std::endl;
    
    file << "Graph: " << testName << " approx: " << sdpSol.val << " best known: " << result << std::endl;
    avgApproxRatio += sdpSol.val/result;
    counter++;
    
    if(sdpSol.val <= result){
      std::cerr << "OK!" << std::endl;
    }else {
      std::cerr << "FAIL!" << std::endl;
      exit(1);
    }
  }
  file << "Average approximation ratio is: " << (avgApproxRatio/(double)counter) << std::endl;
  
  return;
}

