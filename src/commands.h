#ifndef SDP_COMMANDS_HEADER
#define SDP_COMMANDS_HEADER
#include <unordered_map>
#include <functional>
#include <sstream>
#include "tig.h"
#include "brute_solver.h"
#include "sdp_solver.h"

class CommandManager {
  typedef std::unordered_map<std::string, void (CommandManager::*)(std::stringstream&) > CommandMap;

  public:
  CommandManager(TestInstanceGenerator& tig_, BruteSolver& bSolver_, SdpSolver& sdpSolver_, const char* solution_path_);
  void getAndExecuteCommand(std::stringstream&);
  void executeGenerate(std::stringstream&);
  void executeExit(std::stringstream&);
  void executeSolveAll(std::stringstream&);
  void testSdp(std::stringstream&);
  void solveSdp(std::stringstream&);
  void initNameSolutionMap();
  private:
  // TODO implement help
  CommandMap cmdMap = {
    {"solveBrute", &CommandManager::executeSolveAll},
    {"generate", &CommandManager::executeGenerate},
    {"randomTests", &CommandManager::testSdp},
    {"bigTests", &CommandManager::solveSdp},
    {"exit", &CommandManager::executeExit },
  };
  
  TestInstanceGenerator& tig;
  BruteSolver& bSolver;
  SdpSolver& sdpSolver;
  const char* solution_path;
  std::unordered_map<std::string, std::string> graphNameToSolution;
};

#endif
