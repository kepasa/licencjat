#include "sdp_solver.h"
#include "solution.h"
#include <cstdlib>
#include <cstdio>

SdpSolver::SdpSolver(const char* test_path_, const char* sol_path_) : test_path(test_path_), solution_path(sol_path_) {
    this->alpha = 0.87856;
    this->eps = 1e-2;
  };

std::unique_ptr< Graph > SdpSolver::decode(const std::string& path){
  std::fstream s(path, s.binary | s.in  );
  if(!s.is_open()){
    std::cerr << "File " << path << " could not be open!" << std::endl;
    return NULL;
  }

  int v, e, v1, v2, w;
  s >> v >> e;

  auto G = std::make_unique< Graph > (v, e);
//  std::cerr << "file: " << path << " v: " << v << " e: " << e << std::endl;

  for(int i = 0; i < e; ++i){
    s >> v1 >> v2 >> w;
  //  dbg std::cerr << "edge: " << v1 << " " << v2 << " w: " << w << std::endl;
    (*(G->G))[v1].push_back(Edge(v2,w));
    (*(G->G))[v2].push_back(Edge(v1,w));
  }

  return G;
}

Solution SdpSolver::solve(const std::string& filename){
  auto G = decode(filename);

  /*
   * The problem and solution data.
   */

  struct blockmatrix C;
  double *b;
  struct constraintmatrix *constraints;

  /*
   * Storage for the initial and final solutions.
   */

  struct blockmatrix X,Z;
  double *y;
  double pobj,dobj;

  /*
   * blockptr will be used to point to blocks in constraint matrices.
   */

  struct sparseblock *blockptr;

  /*
   * A return code for the call to easy_sdp().
   */

  int ret;

  /*
   * The first major task is to setup the C matrix and right hand side b.
   */

  /*
   * First, allocate storage for the C matrix.  We have three blocks, but
   * because C starts arrays with index 0, we have to allocate space for
   * four blocks- we'll waste the 0th block.  Notice that we check to 
   * make sure that the malloc succeeded.
   */

  C.nblocks=1;
  C.blocks=(struct blockrec *)malloc(2*sizeof(struct blockrec));
  if (C.blocks == NULL)
  {
    printf("Couldn't allocate storage for C!\n");
    exit(1);
  };
  /*
   * Setup the first block.
   */

  C.blocks[1].blockcategory=MATRIX;
  C.blocks[1].blocksize=G->v;
  C.blocks[1].data.mat=(double *)malloc((G->v)*(G->v)*sizeof(double));
  if (C.blocks[1].data.mat == NULL)
  {
    printf("Couldn't allocate storage for C!\n");
    exit(1);
  };

  /*
   * Put the entries into the first block.
   */ 
  for(int i = 1; i <= G->v; ++i){
    for(int j = 1; j <= G->v; ++j){
      C.blocks[1].data.mat[ijtok(i, j, G->v)]=0.0;
    }
  }

  for(int i = 1; i <= G->v; ++i){

    double sum = 0;
    for(Edge& e : (*(G->G))[i]){
      sum += e.w;
      //if(e.v > i){
      C.blocks[1].data.mat[ijtok(i, e.v, G->v)]=-0.25*e.w;
      //}
    }
    C.blocks[1].data.mat[ijtok(i, i, G->v)]=0.25*sum;
  }
  /*
   * Allocate storage for the right hand side, b.
   */

  b=(double *)malloc((G->v+1)*sizeof(double));
  if (b==NULL)
  {
    printf("Failed to allocate storage for a!\n");
    exit(1);
  };

  /*
   * Fill in the entries in b.
   */

  for(int i = 1; i <= G->v; ++i){
    b[i] = 1.0;
  }
  /*
   * The next major step is to setup the two constraint matrices A1 and A2.
   * Again, because C indexing starts with 0, we have to allocate space for
   * one more constraint.  constraints[0] is not used.
   */

  constraints=(struct constraintmatrix *)malloc(						(G->v+1)*sizeof(struct constraintmatrix));
  if (constraints==NULL)
  {
    printf("Failed to allocate storage for constraints!\n");
    exit(1);
  };

  for(int i = 1; i <= G->v; ++i){
    constraints[i].blocks = NULL;
    /*
     * Each constraint consists of 1 block, each block contain 1 entry 
     * Allocate space for block 1 of Ai.
     */

    blockptr=(struct sparseblock *)malloc(sizeof(struct sparseblock));
    if (blockptr==NULL)
    {
      printf("Allocation of constraint block failed!\n");
      exit(1);
    };

    /*
     * Initialize block.
     */

    blockptr->blocknum=1;
    blockptr->blocksize=G->v; /// TODO maybe change to G->v?
    blockptr->constraintnum=i;
    blockptr->next=NULL;
    blockptr->nextbyblock=NULL;
    blockptr->entries=(double *) malloc((1+1)*sizeof(double));
    if (blockptr->entries==NULL)
    {
      printf("Allocation of constraint block failed!\n");
      exit(1);
    };
    blockptr->iindices=(int *) malloc((1+1)*sizeof(int));
    if (blockptr->iindices==NULL)
    {
      printf("Allocation of constraint block failed!\n");
      exit(1);
    };
    blockptr->jindices=(int *) malloc((1+1)*sizeof(int));
    if (blockptr->jindices==NULL)
    {
      printf("Allocation of constraint block failed!\n");
      exit(1);
    };
    blockptr->numentries=1;
    blockptr->iindices[1]=i;
    blockptr->jindices[1]=i;
    blockptr->entries[1]=1.0;

    blockptr->next = constraints[i].blocks;
    constraints[i].blocks = blockptr;
  } 

  write_prob("test.dat-s", G->v, G->v, C, b, constraints);
  initsoln(G->v, G->v, C, b, constraints, &X, &y, &Z);

  ret = easy_sdp(G->v, G->v, C, b, constraints, 0.0, &X, &y, &Z, &pobj, &dobj);
  if (ret == 0)
    printf("The objective value is %.7e \n",(dobj+pobj)/2);
  else
    printf("SDP failed.\n");

  Solution sol = randomizedRounding(X, G);

  auto pos = filename.find_last_of('/');
  std::string testName = filename.substr(pos+1);

  std::string save = std::string(this->solution_path);
  save.append("/");
  save.append(testName);
  const char* savePath = save.c_str();

  std::cerr << "trying to write to: " << savePath << " " << this->solution_path << " " << testName << std::endl;
  write_sol((char*)savePath, G->v, G->v, X, y, Z);

  free_prob(G->v,G->v,C,b,constraints,X,y,Z);
  
  sol.sdpVal = pobj;

  std::cerr << testName << "ALL GOOD! exiting solve" << std::endl;
  return sol;
}

void SdpSolver::choleskyDecomposition(double** M, double** V, int v){

  for(int i = 1; i <= v; ++i){
    for(int j = 1; j <= i; ++j){
      V[i][j] = M[i][j];
      for(int k = 1; k < j; ++k){
        V[i][j] -= V[i][k]*V[j][k];
      }
      if(i == j){
        V[i][j] = sqrt(V[i][j]);
      } else {
        V[i][j] /= V[j][j];
      }

    }
  }
  
  dbg {
    double tmp = 0.0;
    for(int i = 1; i <= v; ++i){
      for(int j = 1; j <= v; ++j){
        tmp = 0.0;
        for(int k = 1; k <= v; ++k){
          tmp += V[i][k]*V[j][k];
        }
        
        if(tmp - M[i][j] > 1e-3){
          std::cerr << "i: "  << i << " j: "  << j << "WRONG CHOLESKY tmp: " << tmp << " M: " << M[i][j] << std::endl;
        }
      }
    }
  }
}

Solution SdpSolver::randomizedRounding(blockmatrix X, std::unique_ptr<Graph>& G){
  std::cerr << "X: " << X.nblocks << std::endl;

  int v = X.blocks[1].blocksize;
  Solution sol;
  double** M = new double*[v+1];
  double** V = new double*[v+1];
  for(int i = 0; i <= v; ++i){
    M[i] = new double[v+1];
    V[i] = new double[v+1];
  }

  for(int i = 1; i <= v; ++i){
    for(int j = 1; j <= v; ++j){
      M[i][j] = X.blocks[1].data.mat[ijtok(i, j, v)];
      V[i][j] = 0;
      dbg std::cerr << "i: " << i << " j: " << j << " " << M[i][j] << std::endl;
    }
  }
  for(int i = 1; i <= v; ++i){
    for(int j = 1; j <= v; ++j){
      if(M[i][j] != M[j][i]){
        std::cerr << "NOT SYMMETRIC!" << std::endl;
        exit(1);
      }
    }
  }
  
  choleskyDecomposition(M, V, v); // rows of V are vectors on a sphere
  dbg for(int i = 1; i <= v; ++i){
    for(int j = 1; j <= v; ++j){
      std::cerr << V[i][j] << " ";
    }
    std::cerr << std::endl;
  }
  
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<> distribution{0, 1}; // expected value = 0, mean = 1
  double c = (this->eps * this->alpha / 2)/(1 + this->eps * this->alpha / 2 - this->alpha/2);
  double best_cut = -1.0;
  std::vector<int> bestS;

  for(double i = 0.0; i < (1.0/c); ++i){
    double d = 0.0;
    double tmp;
    std::vector<double> splitter;
    for(int i = 1; i <= v; ++i){
      tmp = distribution(gen);
      d += tmp*tmp;
      splitter.push_back(tmp);
    }
    double sLen = 0;
    d = sqrt(d);
    //std::cerr << "splitter: ";
    for(int i = 0; i < splitter.size(); ++i){
      splitter[i] = splitter[i] / d;
      sLen += splitter[i]*splitter[i];
      //std::cerr << splitter[i] << " ";
    }
    //std::cerr << " slen: " << sqrt(sLen) << std::endl;

    
    std::vector<int> S;
    
    tmp = 0.0;
    for(int i = 1; i <= v; ++i){
      // calculate V[i] * splitter
      tmp = 0.0;
      for(int j = 1; j <= v; ++j){
        tmp += V[i][j] * splitter[j-1];
      }
      if(tmp >= 0.0){
        S.push_back(i);
      }
    }
    
    double thisCut = 0.0;
    for(int v : S){
      for(Edge& e : (*(G->G))[v]){
        bool isInS = false;
        for(int i = 0; i < S.size(); ++i){
          if(S[i] == e.v){
            isInS = true;
            break;
          }
        }
        if(!isInS){
          thisCut += e.w;
        }

      }
    }
    
    if(thisCut > best_cut) {
      best_cut = thisCut;
      bestS = S;
    }
  }
  
  sol.v = v;
  sol.val = best_cut;
  sol.S = std::vector<int>(std::move(bestS));

  for(int i = 0; i <= v; ++i){
    delete [] M[i];
    delete [] V[i];
  }
  delete [] M;
  delete [] V;
  return sol;
}

