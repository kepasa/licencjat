#ifndef SDP_SOLUTION_HEADER
#define SDP_SOLUTION_HEADER
#include <vector>

struct Solution {
  int v;
  double val;
  double sdpVal;
  std::vector<int> S;
};

#endif
