import xml.etree.ElementTree as ET


if __name__ == "__main__":
    print("Parsing expected solutions XML")
    unwantedStrings = ['* Upper bound not available', 'Best Known', 'Upper bound']
    nameCutvalueMap = {}

    tree = ET.parse('../solution/maxcut_bestvalues.html')
    root = tree.getroot()

    #print("Hello {} {}".format(root.tag, root.attrib))

    for child in root:
        if child.tag == 'body':
            root = child
            break;

    for table in root:
        #print("table", table.tag)
        for row in table:
            #print("row", table.tag)
            it = 0
            name = ""
            value = 0
            for cell in row:
                for p in cell:
                    if it > 1:
                        break
                    #if p.text not in unwantedStrings:
                    #print(p.tag, p.text, it)
                    if p.text not in unwantedStrings and it == 0:
                        name = p.text
                    if p.text not in unwantedStrings and it == 1:
                        value = p.text
                        nameCutvalueMap[name] = value
                    it+=1

    #print(nameCutvalueMap)
    with open('../solution/maxcut_bestvalues.txt', 'w') as f:
        for key, value in nameCutvalueMap.items():
            f.write(key.lower() + " " + value + '\n')
        print("Finished parsing")