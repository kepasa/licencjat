\subsection{Problem MAX-CUT}
W tym rozdziale zostanie przedstawiony 0,87856-aproksymacyjny algorytm probabilistyczny dla problemu MAX-CUT. Kluczową techniką w rozwiązaniu tego problemu okaże się relaksacja problemów \emph{ściśle kwadratowych} do \emph{zadań wektorowych}, które z kolei są równoważne zadaniom półokreślonym.

\begin{mydefn}[\textbf{MAX-CUT}]
Dany jest graf nieskierowany \(G=(V,E)\) oraz funkcja wag krawędzi \(w:E\rightarrow\mathbb{Q^+}\). Przekrój maksymalny to taki podział \((S, \overline{S})\) zbioru $V$, który maksymalizuje łączną wagę przekroju wyznaczonego przez ten podział, czyli krawędzi \(\lbrace (u,w)\in E\ \vert\ u\in S \land w \in \overline{S} \rbrace\).
\end{mydefn}

\subsubsection{Reprezentacja w postaci ściśle kwadratowej i wektorowej}
\emph{Programowanie kwadratowe} to problem optymalizacji, w którym funkcja celu i ograniczenia są funkcjami kwadratowymi zmiennych. Jeśli wszystkie jednomiany występują tylko w stopniu 2 lub 0 to zadanie jest \emph{ściśle kwadratowe}.
\begin{mydefn}
Sformułowanie zadania ściśle kwadratowego dla problemu MAX-CUT.\\
Niech \(G=(V,E)\), \(x_i \in \{-1,1\}\) będzie zmienną rzeczywistą dla wierzchołka $v_i$ grafu. Każde przypisanie zmiennym wartości $\{-1,1\}$ definiuje pewien podział $(S, \overline{S})$. Zauważmy, że znalezienie podziału maksymalnego jest równoważne znalezieniu rozwiązania następującej instancji problemu ściśle kwadratowego:
\begin{alignat}{2} 
&\text{zmaksymalizuj} \quad&& \frac{1}{2} \sum_{1\leqslant i<j \leqslant |V|} w_{ij}(1-x_ix_j) \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& x_i^2 = 1,\ i=1\ldots |V| \label{MAXCUTquadratic} \\
& 	  \quad&& x_i \in \mathbb{R} \nonumber
\end{alignat}
\end{mydefn}

\emph{Zadanie wektorowe} to problem optymalizacyjny, w którym zmienne są wektorami $\nu_i \in \mathbb{R}^n$ a funkcja celu i ograniczenia są funkcjami liniowymi iloczynów skalarnych tych wektorów. Zadanie wektorowe jest więc zadaniem liniowym ze zmiennymi zastąpionymi przez iloczyny skalarne wektorów. Zadanie wektorowe jest relaksacją zadań ściśle kwadratowych. Istotnie, zastąpmy każdą zmienną $x_i$ wektorem $\nu_i \in \mathbb{R}^n$, a wyrazy $x_ix_j$ iloczynem skalarnym odpowiadających im wektorów. Dla problemu MAX-CUT relaksacja przybierze następującą formę:
\begin{mydefn}
Sformułowanie zadania wektorowego dla problemu MAX-CUT.
\begin{alignat}{2} 
&\text{zmaksymalizuj} \quad&& \frac{1}{2} \sum_{1\leqslant i<j \leqslant |V|} w_{ij}(1-\nu_i \nu_j) \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& \nu_i \nu_i = 1,\ i=1\ldots |V| \label{MAXCUTvectoro} \\
& 	  \quad&& \nu_i \in \mathbb{R}^n \nonumber
\end{alignat}
Ograniczenia \eqref{MAXCUTvectoro} gwarantują, że wektory \(v_i\) będą się znajdować na n-wymiarowej sferze jednostkowej.
\end{mydefn}
Aby pokazać, że program wektorowy \eqref{MAXCUTvectoro} jest relaksacją programu ściśle kwadratowego \eqref{MAXCUTquadratic} wystarczy zauważyć, że rozwiązanie dopuszczalne programu \eqref{MAXCUTquadratic} jest rozwiązaniem dopuszczalnym programu \eqref{MAXCUTvectoro}. Dla rozwiązania dopuszczalnego $\overline{x_i}$ problemu \eqref{MAXCUTquadratic} przyjmijmy $\overline{\nu_i} = (\overline{x_i},0, \ldots, 0)$. Wtedy $\overline{\nu_i} \overline{\nu_j} = \overline{x_ix_j}$ oraz $\overline{\nu_i}$ są rozwiązaniem dopuszcalnym problemu \eqref{MAXCUTvectoro}.\\ Zadania wektorowe można aproksymować za pomocą programowania półokreślonego z dowolnym błędem addytywnym \(\varepsilon > 0\) w czasie wielomianowym od liczby zmiennych i \(\log (\frac{1}{\varepsilon})\), na przykład za pomocą algorymu elipsoidalnego \cite{ellipsoidMethod}.\\Kolejnym etapem konstrukcji algorytmu aproksymacyjnego jest pokazanie, że każdemu zadaniu wektorowemu odpowiada instancja problemu programowania półokreślonego.

\subsubsection{Reprezentacja w postaci zadania półokreślonego}
W tej sekcji opiszemy sposób konwersji zadania wektorowego do zadania  półokreślonego.

\begin{pthm} \label{sdpvectorequality}
Niech $V$ będzie instancją zadania wektorowego:
\begin{alignat}{2} 
&\text{zmaksymalizuj} \quad&& \sum_{1\leqslant i \leqslant j \leqslant n} a_{ij}\nu_i \nu_j,\ a_{ij}\in \mathbb{R} \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& \sum_{1\leqslant i \leqslant j \leqslant n} b_{ij}^{(k)} \nu_i \nu_j = c_k,\ i \leqslant j,\ b_{ij}^{(k)},c_{k}\in \mathbb{R}  \label{eq:GENERALvectoro} \\
& 	  \quad&& \nu_i \in \mathbb{R}^m \nonumber
\end{alignat}
Istnieje instancja $P$ zadania półokreślonego w postaci jak w \eqref{eq:SDP} równoważna $V$.
\end{pthm}
\begin{proof}
Niech $V$ będzie instancją problemu wektorowego \eqref{eq:GENERALvectoro}. Pokażemy jak stworzyć instancję problemu półokreślonego równoważnego \eqref{eq:SDP}, która będzie miała tą samą wartość funkcji celu dla rozwiązania optymalnego, co program $V$. Niech $\overline{\nu} = \nu_1,\nu_2,\ldots,\nu_n \in \mathbb{R}^m$ będzie rozwiązaniem dopuszczalnym $V$. Zgodnie z \eqref{eq:GENERALvectoro}, jednomiany w funkcji celu $V$ są postaci $a_{ij}\nu_i\nu_j$, gdzie $a_{ij} \in \mathbb{R}$, oraz $i \leqslant j$, a jednomiany w ograniczeniach sa postaci $b_{ij}\nu_i\nu_j$, $b_{ij} \in \mathbb{R}$. Zdefiniujemy macierze $A_1, A_2,\ldots,A_k,C \in \mathbb{R}^{n\times n}$ dla programu półokreślonego \eqref{eq:SDP}. Macierz $C$ definiujemy następująco. Jeśli $a_{ij}$ jest współczynnikiem przy $\nu_i \nu_j$ w funkcji celu zadania $V$, to \[
 C_{ij} = 
  \begin{cases} 
   a_{ij} & \text{jeżeli } i=j \\
   \frac{1}{2}a_{ij}       & \text{jeżeli } i\neq j
  \end{cases}
\]
Funkcja celu $P$ ma wtedy postać $C\bullet X = \sum \limits_i a_{ii}x_{ii} + 2\sum \limits_{i \neq j} \frac{1}{2}a_{ij}x_{ij}$. Macierze $A_1,\ldots, A_k$ definiujemy analogicznie. Rozwiązanie dopuszczalne $\overline{X}$ tego zadania półokreślonego stworzymy z rozwiązania dopuszczalnego $\overline{\nu}$ zadania półokreślonego $V$ w następujący sposób. Niech macierz $W \in \mathbb{R}^{n\times n}$ będzie utworzona z wektorów $\nu_1,\nu_2\ldots \nu_m$ ułożonych kolumnami i niech $\overline{X} = W^{T}W$. Wtedy $X_{ij} = \nu_i \cdot \nu_j$. Z konstrukcji macierzy $A_1, A_2,\ldots,A_k,C$ wynika, że macierz $\overline{X}$ jest rozwiązaniem dopuszczalnym \eqref{eq:SDP} oraz funkcja celu dla macierzy $\overline{X}$ jest równa funkcji celu dla wektorów $\nu_1,\nu_2\,\ldots,\nu_n$ w programie \eqref{eq:GENERALvectoro}.


Pokażemy teraz jak dla zadania półokreślonego $P$ i jego rozwiązania dopuszczalnego $\overline{X}$ skonstruować odpowiadające zadanie wektorowe $V$ i odpowiadające rozwiązanie dopuszczalne $\overline{\nu}$. Z własności macierzy półokreślonych oraz z faktu, że $\overline{X} \succeq 0$ możemy przedstawić $\overline{X}$ w postaci $\overline{X}=W^{T}W$, $W\in \mathbb{R}^{n\times n}$. Możemy potraktować kolumny W jako wektory $\nu_1,\nu_2,\ldots,\nu_n$.
Wykażemy, że jeżeli $\overline{X}$ jest rozwiązaniem dopuszczalnym \eqref{eq:SDP}, to wektory $\nu_1,\nu_2,\ldots,\nu_n$ są rozwiązaniem dopuszczalnym \eqref{eq:GENERALvectoro}. Co więcej, wartość funkcji celu w zadaniu \eqref{eq:GENERALvectoro} dla wektorów $\nu_1,\nu_2,\ldots,\nu_n$ jest równa wartości funkcji celu w zadaniu \eqref{eq:SDP} dla rozwiązania dopuszczalnego $\overline{X}$. Z górnego trójkąta macierzy $C$ oraz $A_1, A_2,\ldots,A_n$ uzyskujemy współczynniki do funkcji liniowych funkcji celu oraz ograniczeń. Dwukrotność współczynnika na pozycji $(i,j)$, $i < j$ w tych macierzach odpowiada współczynnikowi przy iloczynie skalarnym $\nu_i \nu_j,\ i<j$. Współczynniki na pozycjach $(i,i)$ odpowiadają współczynnikom iloczynów skalarnych $\nu_i \nu_i$. W ten sposób otrzymaliśmy instancję problemu wektorowego $V$ oraz dla rozwiązania dopuszczalnego $\overline{X}$ problemu półokreślonego stworzyliśmy rozwiązanie dopuszczalne $\overline{\nu}$ o tej samej wartości funkcji celu przy zachowaniu warunków \eqref{eq:GENERALvectoro}. 
\end{proof}
Zgodnie z twierdzeniem \ref{sdpvectorequality}, możemy skonstruować problem półokreślony  dla MAX-CUT na podstawie problemu wektorowego \eqref{MAXCUTvectoro}.\\
Zaczynając od podstawienia za iloczyny skalarne $\nu_i \nu_j$ komórki $X_{ij}$ otrzymujemy:
\begin{alignat}{2} 
&\text{zmaksymalizuj} \quad&& \frac{1}{2} \sum_{1\leqslant i<j \leqslant |V|} w_{ij}(1-X_{ij}) \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& X_{ii} = 1,\ i=1\ldots |V| \label{SDPmaxcut} \\
& 	  \quad&& X \succeq 0 \nonumber
\end{alignat}
Jest to zadanie przypominające problem programowania półokreślonego, lecz aby otrzymać instancję zgodną z problemem pólokreślonym zdefiniowanym w \eqref{eq:SDP}, należy pozbyć się warunku $i<j$. Zauważmy, że funkcję celu możemy przedstawić następująco:\\
\begin{equation*}
\begin{split}
\frac{1}{2} \sum\limits_{1\leqslant i<j \leqslant |V|} w_{ij}(1-X_{ij})  & = \frac{1}{4} \sum\limits_{i,j \in \{1..|V|\}} w_{ij}(1 - X_{ij}) \\
& = \frac{1}{4}[ \sum\limits_{i,j} w_{ij} - \sum\limits_{i\neq j} w_{ij}X_{ij} ] \\
& = \frac{1}{4}[ \sum\limits_{i}(\sum\limits_{j } w_{ij}) - \sum\limits_{i\neq j} w_{ij}X_{ij} ] \\
& = \frac{1}{4}[ \sum\limits_{i}(\sum\limits_{j } w_{ij})X_{ii} - \sum\limits_{i\neq j} w_{ij}X_{ij} ]
\end{split}
\end{equation*}
\\
Ostatnia równość wynika z faktu, że ograniczenia gwarantują \(X_{ii} = 1\). Zatem funkcja celu jest postaci $C \bullet X$, gdzie macierz $C \in \mathbb{R}^{|V|\times |V|}$ jest zdefiniowana jako:
\[
 C_{ij} = 
  \begin{cases} 
   \sum\limits_{k \in \{1..|V|\}} w_{ik} & \text{jeżeli } i=j \\
   -w_{ij}       & \text{jeżeli } i\neq j
  \end{cases}
\]
Macierze ograniczeń \(A_1,A_2\ldots,A_{|V|}\) są macierzami wypełnionymi zerami z jedynką w komórkach \((A_{i})_{ii} = 1\), a liczby \(b_1,b_2,\ldots,b_{|V|}\) są równe 1. To gwarantuje, że \(A_i \bullet X = X_{ii} = b_i = 1\). Tym samym otrzymujemy instancję programowania półokreślonego zgodną z definicją \eqref{eq:SDP}.

\subsubsection{Algorytm zaokrąglania randomizowanego} \label{algorytmRandomizowany}
Pokażemy, że następujący algorytm jest 0.87856-aproksymacją MAX-CUT.
\begin{enumerate} \label{randrounding}
\item Znajdź wektory jednostkowe \(\nu_1,\nu_2,\ldots,\nu_n\) maksymalizujące funkcję celu zadania \eqref{MAXCUTvectoro} (poprzez rozwiązanie programu półokreślonego \eqref{SDPmaxcut}).
\item Wybierz losowy wektor $\textbf{r}$ z n-wymiarowej sfery jednostkowej.
\item Zwróć podział $S=\{v_i\ \vert\ \nu_i\cdot \textbf{r} \geqslant 0\}$, $\overline{S} = V\setminus S$.
\end{enumerate}

Zbiór S tworzą wektory leżące po jednej stronie hiperpłaszczyzny H, której wektorem normalnym jest $\textbf{r}$. Z zadania wektorowego \eqref{MAXCUTvectoro}, jeśli przez $\theta_{ij}$ oznaczymy kąt pomiędzy wektorami jednostkowymi $\nu_i$ oraz $\nu_j$, to wartość funkcji celu $OPT_{\nu} = \frac{1}{2} \sum\limits_{1 \leqslant i < j \leqslant n} w_{ij}(1 - cos(\theta_{ij}))$. W kolejnym kroku wykażemy, że $\textbf{E}(W) \geqslant 0.87856\cdot  OPT_{\nu}$, gdzie $\textbf{E}(W)$ jest wartością oczekiwaną zmiennej losowej $W$ odpowiadającej wartości przekroju otrzymanego przez powyższy algorytm. Aby to zrobić potrzebne są następujące lematy:
\begin{lemma} \label{probIsEqualToAngle} \mbox{}\\
$\textbf{P}$(wierzchołek $v_i$ trafił do innego zbioru niż $v_j$)$ = \frac{\theta_{ij}}{\pi}$.
\end{lemma}
\begin{proof}
Niech $O$ będzie odcinkiem powstałym z przecięcia hiperpłaszczyzny $H$ z płaszczyzną $P$ wyznaczoną przez wektory $\nu_i$ oraz $\nu_j$. Wierzchołek $v_i$ znajdzie się w innym zbiorze niż $v_j$, jeżeli wektory $\nu_i$ oraz $\nu_j$ znajdą się po różnych stronach odcinka $O$. Odcinek $O$ będzie średnicą okręgu powstałego w wyniku rzutu sfery jednostkowej $S_n$ na płaszczyznę $P$. Odcinek $O$ jest jednoznacznie wyznaczony przez przez wybór dwóch wektorów normalnych $\textbf{r}$. Prawdopodobieństwo, że $O$ przecina łuk pomiędzy wektorami $\nu_i$, $\nu_j$ jest zatem równe $\frac{\theta_{ij}}{\pi}$.
\end{proof}

\begin{lemma} \label{alphaDef} \mbox{} \\
Przyjmijmy $\alpha = \frac{2}{\pi} \min\limits_{0 < \theta \leqslant \pi} \frac{\theta}{1-cos(\theta)}$. Wtedy $\alpha > 0.87856$.
\end{lemma}
\begin{proof}
Dla $0 < \theta \leqslant \frac{\pi}{2}$ pokażemy, że $\frac{2}{\pi} \frac{\theta}{1-cos(\theta)} \geqslant 1$. Jako, że $1-cos(\theta) > 0$, możemy przemnożyć obie strony nierówności przez $1-cos(\theta)$. Otrzymujemy $f(\theta) = cos(\theta) + \frac{2\theta}{\pi} - 1 \geqslant 0$. Aby udowodnić tę nierówność wystarczy zauważyć, że pochodna lewej strony wynosi $\frac{2}{\pi} - sin(\theta)$. Jest więc malejąca na przedziale oraz $\frac{2}{\pi} - sin(\frac{\pi}{2}) < 0$ i $\frac{2}{\pi} - sin(0) > 0$. Tak więc wystarczy sprawdzić, że $f(\frac{\pi}{2}) \geqslant 0$ oraz $\lim\limits_{\theta \to 0^+} f(\theta) \geqslant 0$.\\
Dla $\frac{\pi}{2} < \theta \leqslant \pi$ skorzystamy z wklęsłości funkcji $g(\theta) = 1 - cos(\theta)$. Istotnie, $g^{''}(\theta) = cos(\theta) \leqslant 0$ w danym przedziale. Z własności wklęsłości $g(\theta) \leqslant g^{'}(\theta_0)(\theta - \theta_0) + f(\theta_0)$ dla dowolnego $\theta_0 \in (\frac{\pi}{2}, \pi]$. Rozwijając nierówność otrzymujemy $1 - cos(\theta) \leqslant \theta sin(\theta_0) + 1 - cos(\theta_0) - \theta_0sin(\theta_0)$. Dla $\theta_0 = 2.331122$ dla którego $1 - cos(\theta_0) - \theta_0sin(\theta_0) < 0$ nierówność ma postać $1 - cos(\theta) < \theta sin(\theta_0)$. \\Z definicji $\alpha > \frac{2}{\pi}\frac{\theta}{\theta sin(\theta_0)} =  \frac{2}{\pi sin(\theta_0)} > 0.87856  $.
\end{proof}
\begin{lemma} \label{angleAlphaLemma} \mbox{} \\
Dla każdego $\theta \in (0, \pi]$ zachodzi $\ \frac{\theta}{\pi} \geqslant \alpha \frac{1-cos(\theta)}{2}$.
\end{lemma}
\begin{proof}
Korzystając z definicji $\frac{2}{\pi} \cdot \frac{\theta}{1-cos(\theta)} \geqslant \alpha$, tak więc \\
$\frac{\theta}{\pi} = \frac{2}{\pi} \cdot \frac{\theta}{1-cos(\theta)} \cdot  \frac{1-cos(\theta)}{2} \geqslant \alpha \frac{1-cos(\theta)}{2}$.
\end{proof}
\pagebreak
\begin{lemma} \label{algorithmExpectedValue} \mbox{} \\
$\textbf{E}(W) \geqslant \alpha OPT_{\nu}$.
\end{lemma}
\begin{proof}
\begin{equation*}
\begin{split}
\textbf{E}(W) & = \sum\limits_{1\leqslant i < j \leqslant n} w_{ij} \textbf{P}(\text{wierzchołek $v_i$ trafił do innego zbioru niż $v_j$})\\
 & = \sum\limits_{i < j} w_{ij} \frac{\theta_{ij}}{\pi} \geqslant \sum\limits_{i < j} w_{ij}\alpha  \frac{1 - cos(\theta_{ij})}{2} = \alpha \frac{1}{2} \sum\limits_{i < j} w_{ij}(1-cos(\theta_{ij})) = \alpha OPT_{\nu}.
\end{split}
\end{equation*}

\end{proof}

Ostatnim krokiem dowodu jest pokazanie, że algorytm zwraca z dużym prawdopodobieństwem przekrój o wadze nie mniejszej niż $0.87856\cdot OPT_{cut}$, gdzie $OPT_{cut}$ jest przekrojem optymalnym (i wartością funkcji celu rozwiązania optymalnego programu kwadratowego dla MAX-CUT).\\
Powtarzając algorytm odpowiednią liczbę razy, algorytm nasz zwraca przekrój o wadze co najmniej $\alpha OPT_v$ z prawdopodobieństwem bliskim 1.


Niech $W_T$ będzie sumą wszystkich krawędzi w grafie $G$. Dobieramy liczbę rzeczywistą $d$ tak aby $\textbf{E}(W)=dW_T$. Niech $p = \textbf{P}(W < (1-\epsilon)dW_t)$ dla pewnej liczby rzeczywistej $\epsilon > 0$. Jako, że $dW_t = \textbf{E}(W) = W_1p_1 + \ldots + W_kp_k + \ldots + W_mp_m$, gdzie $W_i$ są wszystkimi możliwymi wartościami przekroju, a $p_i$ odpowiadającymi im prawdopodobieństwami oraz $W_k < (1-\epsilon)dW_T \leqslant W_{k+1}$, to $dW_T \leqslant p(1-\epsilon)dW_T + (1-p)W_T$. Z tej nierówności możemy wywnioskować, że $p \leqslant \frac{1-d}{1-d+d\epsilon}$.\\
Z nierówności $W_T \geqslant \textbf{E}(W) = dW_T \geqslant \alpha\cdot OPT_{\nu} \geqslant \alpha\cdot OPT_{cut} \geq \alpha \frac{W_T}{2}$ otrzymujemy $d \geqslant \frac{\alpha}{2}$. Latwo pokazać, że $p \leqslant  \frac{1-d}{1-d+d\epsilon} \leqslant \frac{1-\frac{\alpha}{2}}{1-\frac{\alpha}{2}+\frac{\alpha}{2}\epsilon} = 1 - \frac{\frac{\alpha}{2}\epsilon}{1-\frac{\alpha}{2}+\frac{\alpha}{2}\epsilon}$. Niech $\gamma = \frac{\frac{\alpha}{2}\epsilon}{1-\frac{\alpha}{2}+\frac{\alpha}{2}\epsilon}$. Wykonując algorytm $\frac{1}{\gamma}$ i zapamiętując najlepszy wynik $W_{best}$ razy otrzymujemy następujące szacowanie:\\
$\textbf{P}(W_{best} \geqslant (1-\epsilon)dW_{T}) = 1 - \textbf{P}(W_{best} < 1-\epsilon)dW_{T} \text{ w $\frac{1}{\gamma}$ próbach}) = 1 - (1-\gamma)^{\frac{1}{\gamma}} > 1 - \frac{1}{e}$. Jako, że z poprzednich lematów wiemy, że $dW_T \geqslant \alpha\cdot OPT_{cut} > 0.87856 \cdot OPT_{cut}$, możemy dobrać $\epsilon > 0$ tak aby $(1-\epsilon)dW_T \geqslant 0.87856 \cdot OPT_{cut}$.

\begin{pthm}
Powtarzając algorytm z sekcji \ref{algorytmRandomizowany} $\frac{1}{\gamma}$ razy znaleziony przekrój jest równy co najmniej $0.87856\cdot OPT_{cut}$ z prawdopodobieństwem $1 - \frac{1}{e}$.
\end{pthm}
\subsubsection{Generalizacja na grafy z ujemnymi wagami krawędzi}
Do tej pory skupialiśmy się na grafach $G=(V,E)$ dla których funkcja wag krawędzi miała następującą postać \(w:E\rightarrow\mathbb{Q^+}\). Analiza algorytmu aproksymacyjnego korzysta silnie z tej własności, szczególnie w lemacie \ref{algorithmExpectedValue}. Podamy teraz alternatywę tego lematu dla grafu w którym nie zakładamy nieujemności wag krawędzi. \\
Na początek udowodnimy alternatywę lematu \ref{angleAlphaLemma}.
\begin{lemma} \label{negativeWangleLemma} \mbox{} \\
Dla każdego $ \phi \in (0, \pi]$ zachodzi $\ \frac{1 - \phi}{\pi} \geqslant \alpha \frac{1+cos(\theta)}{2}$.
\end{lemma}
\begin{proof}
Przeformułujmy lemat \ref{angleAlphaLemma} w następujący sposób
\begin{equation*}
\forall z \in [-1, 1],\ \frac{arccos(z)}{\pi} \geqslant \alpha \frac{1-z}{2}
\end{equation*}
Podstawiając za $y=-z$ oraz z faktu, że $arccos(-y) = \pi - arccos(y)$ otrzymujemy
\begin{equation*}
\forall y \in [-1, 1],\ \frac{\pi - arccos(y)}{\pi} \geqslant \alpha \frac{1+y}{2}
\end{equation*}
Wstawiając $y = cos(\phi)$ otrzymujemy tezę.
\end{proof}

\begin{lemma} \label{negativeWeights} \mbox{} \\
Niech 
\[
 W_{-} = \sum\limits_{1 \leqslant i < j \leqslant n} 
  \begin{cases} 
   w_{ij} & \text{jeżeli } w_{ij} < 0 \\
   0       & \text{jeżeli } w_{ij} \geqslant 0
  \end{cases}
\] będzie sumą krawędzi o ujemnej wadze. Wtedy:
\begin{equation*}
(\textbf{E}(W) - W_{-}) \geqslant \alpha[\frac{1}{2} \sum\limits_{1 \leqslant i < j \leqslant n}w_{ij}(1 - \nu_i \nu_j) - W_{-}]
\end{equation*}
\end{lemma}
\begin{proof}
Niech $\phi_{ij}$ będzie kątem pomiędzy wektorami $\nu_i$ oraz $\nu_j$. Wówczas $\textbf{E}(W) - W_{-}$ możemy zapisać jako:
\begin{equation*}
\sum\limits_{i<j\ \vert\ w_{ij} > 0} w_{ij}\frac{\phi_{ij}}{\pi} + \sum\limits_{i<j\ \vert\ w_{ij} < 0} \lvert w_{ij} \rvert (1 - \frac{\phi_{ij}}{\pi}).
\end{equation*}
Podobnie $\frac{1}{2} \sum\limits_{1 \leqslant i < j \leqslant n}w_{ij}(1 - \nu_i \nu_j) - W_{-}$ zapisujemy jako
\begin{equation*}
\begin{multlined}
\sum\limits_{i<j\ \vert\ w_{ij} > 0} w_{ij}\frac{1 - cos(\phi_{ij})}{2} - \sum\limits_{i<j\ \vert\ w_{ij} < 0} \lvert w_{ij} \rvert\frac{1 - cos(\phi_{ij})}{2} + \sum\limits_{i<j\ \vert\ w_{ij} < 0} \lvert w_{ij} \rvert = \\
= \sum\limits_{i<j\ \vert\ w_{ij} > 0} w_{ij}\frac{1 - cos(\phi_{ij})}{2} - \sum\limits_{i<j\ \vert\ w_{ij} < 0} \lvert w_{ij} \rvert\frac{1 + cos(\phi_{ij})}{2}.
\end{multlined}
\end{equation*}
Korzystając z lematów \ref{angleAlphaLemma} i \ref{negativeWangleLemma} otrzymujemy tezę.
\end{proof}
Z lematu \ref{negativeWeights} wynika, że w przypadku dopuszczenia ujemnych wag krawędzi, współczynnik aproksymacji zależy od sumy krawędzi o ujemnych wagach.

\subsection{MAX-2SAT}
Podobny algorytm zaokrąglania randomizowanego pozwala na 0.87856-aproksymację problemu MAX-2SAT.

\begin{mydefn}[\textbf{MAX-2SAT}]
Niech $\Psi$ będzie kolekcją klauzul, gdzie każda z klauzul $C$ jest alternatywą co najwyżej dwóch literałów zmiennych boolowskich ze zbioru ${x_1, \ldots, x_n}$. Optymalne rozwiązanie problemu \emph{MAX-2SAT} to takie wartościowanie zmiennych ${x_1, \ldots, x_n}$ które maksymalizuje liczbę spełnionych klauzul z kolekcji $\psi$.
\end{mydefn}

\subsubsection{Reprezentacja w postaci ściśle kwadratowej i wektorowej}
Rozważmy następujący program kwadratowy.
\begin{mydefn}
Niech \(y_i \in \{-1,1\}\) będzie zmienną rzeczywistą dla zmiennej boolowskiej $x_i$ instancji $\psi$ problemu MAX-2SAT oraz $a_{ij},b_{ij} \in \mathbb{R}^{+}$. Wykażemy poniżej, że program MAX-2SAT możan wyrazić za pomocą danego programu ściśle kwadratowego:
\begin{alignat}{2} \label{MAX2SATquadratic}
&\text{zmaksymalizuj} \quad&& \sum \limits_{0\leqslant i<j \leqslant n} [a_{ij}(1-y_iy_j) + b_{ij}(1+y_iy_j)] \nonumber  \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& y_i^2 = 1,\ i=0\ldots n \\
& 	  \quad&& y_i \in \mathbb{R}, \nonumber
\end{alignat}
\end{mydefn}
gdzie $a_{ij}$, $b_{ij}$ są odpowiednio dobrane dla danej formuły $\psi$.\\

Dla każdej zmiennej boolowskiej $x_i$ wprowadzamy odpowiadającą zmienną $y_i \in \{-1, 1\},\ 1\leqslant i \leqslant n$. Wprowadzamy również dodatkową zmienną $y_0 \in \{-1, 1\}$, która determinuje czy $1$ czy $-1$ odpowiada wartości $True$, dokładniej $x_i$ przyjmuje wartość $True$ wtedy i tylko wtedy, gdy $y_i = y_0$. Niech wartość klauzuli $v(C)$ będzie równa $1$, gdy klauzula jest spełniona i $-1$, gdy jest niespełniona. Dla klauzul złożonych z jednego literału otrzymujemy wtedy: $v(x_i) = \frac{1+y_iy_0}{2},\ v(\overline{x_i}) = 1-v(x_i) = \frac{1-y_iy_0}{2}$. Dla klauzul z 2 literałami: $v(x_i \vee x_j) = 1-v(\overline{x_i}\wedge \overline{x_j}) = 1- v(\overline{x_i})v(\overline{x_j}) = 1 -(\frac{1-y_iy_0}{2})(\frac{1-y_jy_0}{2}) = \frac{1}{4}(3 + y_0y_i + y_0y_j - y_0^2 y_iy_j) = \frac{1+y_0y_i}{4} + \frac{1+y_0y_j}{2} + \frac{1-y_iy_j}{4}$. Analogicznie wyrażamy każdą z 4 możliwych typów klauzul składających się z 2 literałów. Kodując klauzule z $\psi$ w taki sposób i grupując odpowiednie wyrazy otrzymujemy współczynniki $a_{ij},b_{ij}$. Tak więc otrzymujemy wyżej podany program ściśle kwadratowy. Zastępując zmienną $y_i$ zmienną wektorową $\nu_i$ dostajemy relaksację wektorową problemu \eqref{MAX2SATquadratic}:
\begin{alignat}{2} \label{MAX2SATvector}
&\text{zmaksymalizuj} \quad&& \sum_{0\leqslant i<j \leqslant n} [a_{ij}(1-\nu_i \nu_j) + b_{ij}(1+\nu_i \nu_j)] \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& \nu_i \nu_i = 1,\ i=0\ldots n \\
& 	  \quad&& \nu_i \in \mathbb{R}^n. \nonumber
\end{alignat}

\subsubsection{Reprezentacja w postaci zadania półokreślonego}
Z twierdzenia \ref{sdpvectorequality} wiemy, że zadaniom wektorowym odpowiadają zadania półokreślone. Tak więc wystarczy, że przedstawimy jak wygląda macierz $C$ zdefiniowana w definicji \eqref{eq:SDP}. Rozpisując funkcję celu programu \eqref{MAX2SATvector} otrzymujemy: \begin{multline*}
\sum_{i,j \in \{0\ldots n\}} [a_{ij}(1-X_{ij}) + b_{ij}(1+X_{ij})] = \sum_{i,j \in \{0\ldots n\}} (a_{ij} + b_{ij}) + \sum_{i \neq j} (a_{ij} - b_{ij})X_{ij} =  \\ = \sum_{i \in \{1\ldots n\}} [ \sum_{j\in \{1\ldots n\}} (a_{ij} + b_{ij}) ] X_{ii} + \sum_{i \neq j} (a_{ij} - b_{ij})X_{ij}. 
\end{multline*}
Tym samym macierz $C$ ma postać:
\[
 C_{ij} = 
  \begin{cases} 
   \sum\limits_{k \in \{0\ldots n\}} a_{ik} + b_{ik} & \text{jeżeli } i=j \\
   a_{ij} - b_{ij}       & \text{jeżeli } i\neq j.
  \end{cases}
\]

\subsubsection{Algorytm zaokrąglania randomizowanego}
Zastosujemy algorytm podobny do algorytmu zaokrąglania randomizowanego dla MAX-CUT. Niech $\nu_0, \ldots, \nu_n$ będą wektorami otrzymanymi z rozwiązania zadania wektorowego \eqref{MAX2SATvector} oraz wektor $\textbf{r}$ będzie wektorem jednostkowym ze sfery $S_n$ wybranym z rozkładem jednostajnym. Niech $y_i = 1$ wtedy i tylko wtedy, gdy $\textbf{r} \cdot a_i \geqslant 0,\ 0 \leqslant i \leqslant n$. Pokażemy, że $\textbf{E}(W) \geqslant \alpha OPT_{\nu}$, dla zmiennej losowej $W$ oznaczającej ilość spełnionych klauzul, gdzie $OPT_{\nu}$ jest wartością funkcji celu w rozwiązaniu optymalnym zadania wektorowego \eqref{MAX2SATvector}.
\begin{lemma} \mbox{}\\
$\textbf{E}(W) \geqslant \alpha OPT_{\nu}$.
\end{lemma}
\begin{proof}
$\textbf{E}[W] = 2 \sum\limits_{0 \leqslant i < j \leqslant n} a_{ij} \textbf{P}(y_i = y_j) + b_{ij}\textbf{P}(y_i \neq y_j)$. Dla $\theta_{ij}$ będącego kątem pomiędzy wektorami $\nu_i$ oraz $\nu_j$ z lematu \ref{angleAlphaLemma} otrzymujemy $\textbf{P}(y_i \neq y_j) = \frac{\theta_{ij}}{\pi} \geqslant \alpha \frac{1 - cos(\theta_{ij})}{2}$. Podstawiając $\phi = \pi - \theta$ otrzymujemy $\textbf{P}(y_i = y_j) = 1 - \frac{\theta_{ij}}{\pi} \geqslant \alpha \frac{1 + cos(\theta_{ij})}{2}$. Stąd $\textbf{E}[W] \geqslant \alpha \sum\limits_{0 \leqslant i < j \leqslant n} a_{ij}(1+cos(\theta_{ij}) ) + b_{ij}(1-cos(\theta_{ij})) \geqslant \alpha OPT_{\nu}$.
\end{proof}

Powtarzając algorytm zaokrąglania randomizowanego dla problemu \eqref{MAX2SATvector} analogiczny do algorytmu z sekcji \ref{algorytmRandomizowany} otrzymamy wartościowanie spełniające co najmniej $0.87856\cdot OPT$ klauzul $\psi$, gdzie $OPT$ jest rozwiązaniem optymalnym problemu MAX-2SAT dla $\psi$.

\subsection{Kolorowanie grafów 3-kolorowalnych}

\begin{mydefn}
Niech $G=(V,E)$ będzie grafem. \textit{k-kolorowanie} grafu $G$ to funkcja $f:V\rightarrow [k]$ taka, że $\forall (u,v) \in E,\ f(u)\neq f(v)$. Graf $G$ jest \textit{k-kolorowalny} gdy istnieje k-kolorowanie dla tego grafu. \textit{Liczba chromatyczna} to najmniejsza liczba $k$ taka, że graf $G$ jest k-kolorowalny.
\end{mydefn}
Naszym celem jest opisanie algorytmu kolorującego grafy k-kolorowalne przy użyciu niewielkiej liczby kolorów.
Będziemy chcieli podzielić wierzchołki grafu $G$ na $l \geqslant k$ zbiorów w taki sposób, że każda krawędź prowadzi między dwoma różnymi zbiorami. Algorytm będzie mapował wierzchołki na wektory jednostkowe ze sfery $S_n$ tak aby maksymalizować odległości między wierzchołkami połączonymi krawędzią. Prowadzi to do następującego zadania wektorowego:
\begin{mydefn}
Niech $\nu_i \in \mathbb{R}^n$ będzie zmienną wektorową dla każdego wierzchołka $v_i \in V$ grafu $G=(V,E)$. Aby zmaksymalizować dystans pomiędzy wektorami odpowiadającymi wierzchołkom chcemy zminimalizować iloczyn skalarny tych wektorów.

\begin{alignat}{2} \label{GRAPHCOLORvector}
&\text{zminimalizuj} \quad&& t\\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& \nu_i\cdot \nu_j \leqslant t,\ \forall(i,j) \in E \nonumber \\
& 	  \quad&& \nu_i \cdot \nu_i = 1,\ \forall i \in \{1\ldots |V|\}. \nonumber
\end{alignat}
\end{mydefn}
Jak wiemy z twierdzenia \ref{sdpvectorequality} problem ten możemy rozwiązać za pomocą programowania półokreślonego. Pokażemy teraz jak z rozwiązania optymalnego tego problemu skonstruować kolorowanie grafu.

\begin{lemma} \label{coloringUpperBound}
Niech $t_{opt}$ będzie rozwiązaniem optymalnym powyższego problemu. Jeśli $G$ jest k-kolorowalny to $t_{opt} \leqslant \frac{-1}{k-1}$.
\end{lemma}
\begin{proof}
Załóżmy, że mamy podział wierzchołków grafu $G=(V,E)$ na k zbiorów niezależnych. Skonstruujemy k wektorów jednostkowych $\nu_1 \ldots \nu_k \in \mathbb{R}^{k}$  takich, że dla każdego $ i\neq j,\ \nu_i \cdot \nu_j \leq \frac{-1}{k-1}$. Przypisując wierzchołkom z różnych zbiorów odpowiadające wektory otrzymujemy rozwiązanie dopuszczalne powyższego problemu wektorowego z wartością funkcji celu co najwyżej $\frac{-1}{k-1}$, tym samym udowadniając lemat.\\
Wektory $\nu_i$ będziemy konstruować za pomocą indukcji po k. Dla $k=2$ twierdzenie jest trywialne, wystarczy wybrać wektory z kątem pomiędzy nimi równym $\pi$. Załóżmy, że mamy wektory $\nu_1' \ldots \nu_k' \in \mathbb{R}^k$ dla pewnego $k \geqslant 2$ takie, że $\nu_i' \cdot \nu_j' \leqslant \frac{-1}{k-1}$ dla $i \neq j$. Wektory $\nu_1 \ldots \nu_{k+1} \in \mathbb{R}^{k+1}$ konstruujemy następująco:
\begin{itemize}
\item $\nu_{k+1} = (0, \ldots, 1)$
\item $\nu_1\ldots \nu_k = (\eta \nu_1'\ldots \eta\nu_k', -\frac{1}{k})$, gdzie $\eta = \sqrt[]{1-\frac{1}{k^2}}$. 
\end{itemize}
Najpierw pokażemy, że powstałe wektory są wektorami jednostkowymi. Istotnie, \begin{equation*}
\nu_i \cdot \nu_i = \eta^2(\nu_i' \cdot \nu_i') + \frac{1}{k^2} = 1.
\end{equation*} Następnie pokazujemy, że $\nu_i \cdot \nu_j \leqslant -\frac{1}{k}$ dla $1 \leqslant i < j \leqslant k$: 
\begin{equation*}
\nu_i \cdot \nu_j = \eta^2(\nu_i' \cdot \nu_j') + \frac{1}{k^2} \leqslant (1-\frac{1}{k^2})(\frac{-1}{k-1}) + \frac{1}{k^2} = -\frac{k+1}{k^2} + \frac{1}{k^2} = - \frac{1}{k}.
\end{equation*} Ostatecznie zauważmy również, że $\nu_i \cdot \nu_{k+1} = -\frac{1}{k}$ dla $i \leqslant k$, co jest oczywiste.
\end{proof}
Kolejny lemat pokazuje, że w całej klasie grafów k-kolorowalnych ograniczenie z lematu \ref{coloringUpperBound} jest możliwie najlepsze.
\begin{lemma}
Niech $t_{opt}$ będzie rozwiązaniem optymalnym powyższego problemu. Jeśli $G$ zawiera klikę wielkości k, to $t_{opt} \geqslant \frac{-1}{k-1}$
\end{lemma}
\begin{proof}
Niech $\nu_1 \ldots \nu_k \in \mathbb{R}^{n}$ będą wektorami jednostkowymi. Rozważmy iloczyn skalarny $(\sum_{i=1}^{k} \nu_i) \cdot (\sum_{i=1}^{k} \nu_i) = \sum_{i=1}^{k} \nu_i \cdot \nu_i + \sum_{i\neq j} \nu_i \cdot \nu_j = k + (k-1)k\overline{r}$, gdzie $\overline{r}$ jest średnią iloczynów skalarnych dwóch różnych wektorów. Jako, że iloczyn skalarny wektora z samym sobą to długość wektora, to powstała suma jest nieujemna: $k + k(k-1)\overline{r} \geqslant 0$. Stąd $\overline{r} \geqslant -\frac{1}{k-1}$, oraz $max_{i\neq j} \nu_i \cdot \nu_j \geqslant -\frac{1}{k-1}$. Załóżmy, że graf $G=(V,E)$ zawiera k-klikę. Rozważmy k wektorów jednostkowych w rozwiązaniu optymalnym, które odpowiadają wierzchołkom w klice. Spełniają one powyższą nierówność. Jako, że między każdymi dwoma z tych wierzchołków jest krawędź, to istnieje iloczyn skalarny o wartości co najmniej $\overline{r} \geqslant  -\frac{1}{k-1}$.
\end{proof}
\subsubsection{Algorytm zaokrąglania randomizowanego}
Rozważmy następujący algorytm randomizowany dla grafu 3-kolorowalnego \mbox{$G=(V,E)$}. Niech $t = \log_3 \Delta +1$, gdzie $\Delta$ jest maksymalnym stopniem wierzchołka w grafie G.
\begin{itemize}
\item Wybierz t losowych wektorów jednostkowych $\alpha_1, \ldots, \alpha_t$ z rozkładem jednorodnym.
\item Podziel graf na $2^t$ zbiorów. W tyn samym zbiorze znajdują się wierzchołki $v_j$ o tej samej wartości wektora $(sgn(\alpha_1 \cdot \nu_j),\ldots sgn(\alpha_t \cdot \nu_j))$.
\item Dla każdego zbioru z podziału uzyskanego we wcześniejszym kroku, jeżeli w tym zbiorze  istnieją w nim krawędzie, to wywołaj się rekurencyjnie na tym zbiorze.
\end{itemize}
Jeśli graf jest 3-kolorowalny to z lematu \ref{coloringUpperBound} istnieje rozwiązanie problemu wektorowego dla którego $\nu_i \cdot \nu_j \leqslant -\frac{1}{2}$ dla $(i,j) \in E$. Stąd kąt pomiędzy $\nu_i$ oraz $\nu_j$ wynosi co najmniej $\frac{2\pi}{3}$.
\begin{lemma}
Niech $M$ będzie zmienną losową odpowiadającą liczbie monochromatycznych krawędzi. Wtedy $\textbf{E}[M] \leqslant \frac{n}{4}$ po dowolnej iteracji algorytmu.
\end{lemma}
\begin{proof}
Rozważmy krawędź $(u,v)\in E$. Wybierzmy j takie, że $1 \leqslant j \leqslant t$. Jako, że kąt pomiędzy wektorami $\nu_u, \nu_v$ wynosi co najmniej $\frac{2\pi}{3}$, to przeprowadzając analogiczne rozumowanie jak w lemacie \ref{probIsEqualToAngle} otrzymujemy $\textbf{P}(sgn(\alpha_j\cdot \nu_u) = sgn(\alpha_j\cdot \nu_v)) \leqslant \frac{1}{3}$. Stąd $\textbf{P}((u,v)\  monochromatyczna) = \textbf{P}(\forall j\ sgn(\alpha_j\cdot \nu_u) = sgn(\alpha_j\cdot \nu_v)) \leqslant \frac{1}{3^t} = \frac{1}{3\Delta}$. Ostatecznie liczba monochromatycznych krawędzi wynosi co najwyżej $\frac{n\Delta}{2} \frac{1}{3\Delta} \leqslant \frac{n}{4}$.
\end{proof}
Z powyższego lematu wiemy, że po każdej iteracji pozostaje co najwyżej $\frac{n}{2}$ wierzchołków które są końcami monochromatycznych krawędzi. Stąd z wysokim prawdopodobieństwem algorytm kończy się po $O(log\ n)$ iteracji. Możemy przejść do analizy liczby użytych kolorów. W każdej iteracji używamy co najwyżej $2^t$ nowych kolorów. Zakładając, że wykonamy $O(log\ n)$ iteracji, całkowita liczba kolorów wynosi $2^t(log\ n) \approx 2^{(log_3\Delta)} log\ n = \Delta^{log_3 2} log\ n = \Delta^{0.63}log\ n = O(n^{0.63} log\ n)$. Możemy poprawić tę aproksymację ograniczając z góry $\Delta$ parametrem $\Delta^*$ lepszym niż $n$. Prowadzi to do następującego algorytmu:

Otrzymamy następujący algorytm:
\begin{itemize}
\item Wybierz wierzchołek o stopniu większym niż $\Delta^*$. Pokoloruj 3 kolorami wierzchołek wraz z jego sąsiedztwem i usuń je z grafu.
\item Powtarzaj 1 krok tak długo jak są wierzchołki o stopniu większym niż $\Delta^*$.
\item Zastosuj wyżej opisany algorytm zaokrąglania randomizowanego na wynikowym grafie, w którym nie ma wierzchołków stopnia większego niż $\Delta^*$.
\end{itemize}
Liczba kolorów użytych w krokach 1 i 2 wynosi co najwyżej $\frac{3n}{\Delta^*}$. Oczekiwana liczba kolorów użytych w kroku 3 wynosi $(\Delta^*)^{0.63}log\ n$. Aby zminimalizować ich sumę wystarczy wybrać $\Delta^* = n^{\frac{1}{1.63}}$, co daje $O(n^{0.39})$-kolorowanie grafu 3-kolorowalnego.
%\todo[]{dodać opis programu}$
