\subsection{A Proof of Bijection}
\begin{pthm}\pthmlabel{bijection}
  The function \(f:\R\to\R\) defined by
  \begin{equation}\eqlabel{deff}
    f(x)=\begin{cases}
      2;& x=3\\
      \frac{4x+3}{2x-6};& x\neq3
    \end{cases}
  \end{equation}
  is a bijection.
\end{pthm}
\begin{proof}
  We will prove that the function, \(f\), defined by \eqref{deff} is a bijection by
  proving that \(f\) is an injection and that \(f\) is a surjection.

  \paragraph{Injectivity}
  To prove that \(f\) is an injection, we need to show that for any
  \(x_{1}\) and \(x_{2}\) that are elements of the domain of \(f\), if
  \(x_{1}\neq x_{2}\), then \(f(x_{1})\neq f(x_{2})\).  
  We will do this by using the contrapositive.  That is, we will let \(x_{1}\) and \(x_{2}\) be
  arbitrary elements of the domain of \(f\) and show that if
  \(f(x_{1})=f(x_{2})\), then \(x_{1}=x_{2}\).   
  In this case, this means that we assume \(x_{1}\) and \(x_{2}\) are arbitrary
  real numbers and that \(f(x_{1})=f(x_{2})\) and we will show that
  \(x_{1}=x_{2}\).

  Because \(f\) is piecewise defined, we need to consider that we could have \(x_{1}=3\) or \(x_{1}\neq3\),
  and similarly that we could have \(x_{2}=3\) or \(x_{2}\neq3\).  If \(x_{1}=3\) and \(x_{2}=3\),
  then there is nothing to show, since \(x_{1}=3=x_{2}\). Thus we have to consider the following
  cases: \(x_{1}=3\) and \(x_{2}\neq 3\), \(x_{1}\neq 3\) and \(x_{2}= 3\), and \(x_{1}\neq 3\) and
  \(x_{2}\neq 3\). It is easy to see that by simply relabeling \(x_{1}\) and \(x_{2}\), the first
  two cases are really the same. That leaves us with two cases to consider.

\begin{case}[\(x_{1}=3\) and \(x_{2}\neq 3\)] This is a proof by contradiction, and thus we will
  assume that \(x_{1}=3\), \(x_{2}\neq 3\), and \(f(x_{1})=f(x_{2})\), and show that a contradiction
  arises.
    Since \(x_{1}=3\) and \(x_{2}\neq3\), we know that \(f(x_{1})=f(3)=2\) and
    \(f(x_{2})=\frac{4x_{2}+3}{2x_{2}-6}\).  Because we assume that
    \(f(x_1)=f(x_2)\), we thus have
    \begin{equation}
      \frac{4x_{2}+3}{2x_{2}-6}=2.\eqlabel{threeNotThree}
    \end{equation}
    Multiplying each side of \eqref{threeNotThree} by \(2x_2-6\), we obtain
    \begin{align}
      {4x_{2}+3}	&=2(2x_{2}-6)\notag\\
      &=4x_2-12.\eqlabel{impossible}
    \end{align}
    By subtracting \(4x_{2}\) from both sides of 
    \eqref{impossible} we get  \(3=-12\)), which is a contradiction. Thus,
    the \(x_{1}=3\), \(x_{2}\neq3\), and \(f(x_1)=f(x_2)\) cannot occur.
  \end{case}
  
  \begin{case}[\(x_{1}\neq 3\) and \(x_{2}\neq 3\)]
    It now remains to show that if \(x_1\) and \(x_2\) are any real numbers with
    \(x_1\neq3\), \(x_2\neq3\), and \(f(x_1)=f(x_2)\), then \(x_1=x_2\).

    Because \(x_1\neq3\), \(x_2\neq3\), and \(f(x_1)=f(x_2)\), we have that 
    \begin{equation}
      \frac{4x_{1}+3}{2x_{1}-6}=\frac{4x_{2}+3}{2x_{2}-6}.
      \eqlabel{feq}
    \end{equation}

    Multiplying both sides of \eqref{feq} by \((2x_{1}-6)(2x_{2}-6)\) and
    expanding, we get
    \begin{gather}
      (4x_{1}+3)(2x_{2}-6)=(4x_{2}+3)(2x_{1}-6)\notag\\
      \text{and so}\notag\\
      8x_{1}x_{2}-24x_{1}+6x_{2}-18 = 8x_{1}x_{2}+6x_{1}-24x_{2}-18.\eqlabel{expanded}
    \end{gather}
    Subtracting the left hand side of \eqref{expanded} from both sides, we see
    that 
    \begin{align}
      0&=30x_{1}-30x_{2}\notag\\
       &=30(x_{1}-x_{2}).\eqlabel{thirtyNotZero}
    \end{align}
    From \eqref{thirtyNotZero}, since \(30\neq0\), we see that \(x_{1}-x_{2}=0\), and
    thus \(x_{1}=x_{2}\), as desired.  
  \end{case}
  
  Thus, we have shown that the function \(f\) is injective.

  \paragraph{Surjectivity}  We will now show that \(f\) is surjective by showing that for any \(b\) in the
  codomain of \(f\), there is an element \(x\) in the domain of \(f\) so that
  \(f(x)=b\).  Thus, we let \(b\) be an arbitrary element of the codomain of
  \(f\), which is to say that \(b\) is any real number.  

  We will consider two cases for the value of \(b\):  either \(b=2\) or
  \(b\neq2\).

  \begin{case}[\(b=2\)]
    If \(b=2\), then we see that if \(x=3\), then \(f(x)=2=b\) as desired.
  \end{case}

  If \(b\neq{2}\), \(2b-4\neq0\) and so
  \(x=\frac{6b+3}{2b-4}\) is a real number, and thus is in the domain of
  \(f\).  Furthermore,
  \begin{align*}
    f(x) &= f\left(\frac{6b+3}{2b-4}\right)\\
         &=\frac{4\left(\frac{6b+3}{2b-4}\right)+3}{2\left(\frac{6b+3}{2b-4}\right)-6}\\[1ex]
         &=\frac{4\left(6b+3\right)+3\left(2b-4\right)}{2\left(6b+3\right)-6\left(2b-4\right)}\\[1ex]
         &=\frac{24b+12+6b-12}{12b+6-12b+24}\\
         &=\frac{30b}{30}\\
	 &=b.
  \end{align*}
  We have thus shown that for any element \(b\) of the codomain of \(f\), there is
  an element \(x\) of the domain of \(f\) so that \(f(x)=b\), which completes
  our proof that \(f\) is surjective.

  Since we have shown that \(f\) is injective and surjective, this completes our
  proof that \(f\) is a bijection.
\end{proof}