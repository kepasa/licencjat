\subsection{Two Proofs of a Conjecture}
Here we present a conjecture together with both a constructive and a non-constructive proof of the
conjecture.
\begin{pthm}\pthmlabel{irrationals_not_closed}
  The set of irrational real numbers is not closed under exponentiation.  That is,
  there exist irrational numbers \(a\) and \(b\) so that \(a^{b}\) is
  rational.
\end{pthm}

\subsubsection{A Lemma}
Before we begin our proofs of \pthmref{irrationals_not_closed}, we will prove a lemma that will be
needed in the constructive proof.  (It is not needed in the non-constructive
proof.)

\begin{lemma}
  The logarithm base \(2\) of \(9\), \(\log_{2}(9)\), is irrational.\lemmalabel{log}
\end{lemma}
\begin{proof}[Proof of \Lemmaref{log}]
  We will prove that the logarithm base 2 of 9 is irrational by means of a proof
  by contradiction.  Thus, we will assume that \(\log_{2}(9)\) is rational, and
  we will show that this gives rise to a contradiction.

  If \(\log_{2}(9)\) is rational, there must exist (because of what it means to be a rational
  number) integers \(p\) and \(q\), with \(q\neq0\), so that \(\log_{2}(9)=\nicefrac{p}{q}\).  This
  means (from properties of logarithms) that 
  \begin{equation}
    2^{\nicefrac{p}{q}}=9.
    \eqlabel{log}
  \end{equation}

  Furthermore, because \(9>2\), we must have that \(\nicefrac{p}{q}>1>0\), and we can assume that
  both \(p\) and \(q\) are positive integers, since otherwise they would both have to be negative
  and we would use instead the integers \(-p\) and \(-q\).  

  Raising each side of \eqref{log} to the \(q\) power, we obtain
  \begin{equation}
    2^{p}=9^{q}.  \eqlabel{exp}
  \end{equation}

  Because \(p\) and \(q\) are positive integers, both sides of \eqref{exp} are
  integers.  Furthermore, the integer on the left, \(2^{p}\), must be even, and
  the number on the right, \(9^{q}\), is odd.  However, no integer can be both
  even and odd, and so we have arrived at a contradiction.  Therefore, we have
  proven that \(\log_{2}(9)\) is irrational.
\end{proof}

\subsubsection{A Non-Constructive Proof of \pthmref{irrationals_not_closed}}
\begin{proof}[Non-Constructive Proof of \pthmref{irrationals_not_closed}]
  We will show that there must be irrational numbers \(a\) and \(b\) such that
  \(a^{b}\) is rational by considering two possibilities for the real number
  \( \left(\sqrt{3}\right)^{\sqrt{2}}\), namely that this number must either be irrational or
  rational.
  \begin{case}[\(\left(\sqrt{3}\right)^{\sqrt{2}}\text{ rational}\)]  We have shown previously that
      \(\sqrt{2}\) and \(\left(\sqrt{3}\right)\) are irrational.  If
      \(\left(\sqrt{3}\right)^{\sqrt{2}}\) is rational, then we can set \(a=\left(\sqrt{3}\right)\) and
      \(b=\sqrt{2}\), and have that 
      \begin{equation*}
      a^{b}=\left(\sqrt{3}\right)^{\sqrt{2}}
      \end{equation*} 
      so that \(a\) and \(b\) are irrational and \(a^{b}\) is rational, and so our theorem is true.
    \end{case}

    \begin{case}[\(\left(\sqrt{3}\right)^{\sqrt{2}}\text{ irrational}\)]
      If \(\left(\sqrt{3}\right)^{\sqrt{2}}\) is irrational, then we can set
      \(a=\left(\sqrt{3}\right)^{\sqrt{2}}\) and \(b=\sqrt{2}\), and we have 
      \begin{align*}
      a^{b}&= \left(\left(\sqrt{3}\right)^{\sqrt{2}} \right)^{\sqrt{2}}\\
      &= \left(\sqrt{3}\right)^{ \sqrt{2}\cdot\sqrt{2} }\\
      &= \left(\sqrt{3}\right)^{2}\\
      &=3
      \end{align*}
      and \(3\) is rational, so we have found \(a\) and \(b\) irrational with
      \(a^{b}\) rational, so our theorem is true.
    \end{case}
  Since we have shown that whether \(\left(\sqrt{3}\right)^{\sqrt{2}}\) is rational or irrational, there exist irrational numbers
  \(a\) and \(b\) with \(a^{b}\) rational, we have proven our theorem.
\end{proof}

\subsubsection{A Constructive Proof of \pthmref{irrationals_not_closed}}
\begin{proof}[Constructive Proof of \pthmref{irrationals_not_closed}]  
  We will show that there exist irrational numbers
  \(a\) and \(b\) with the property that \(a^{b}\) is rational by exhibiting
  such numbers.  Let \(a=\sqrt{2}\) and let \(b=\log_{2}(9)\).  We have shown
  previously that \(\sqrt{2}\) is irrational, and by \Lemmaref{log} we know that
  \(\log_{2}(9)\) is irrational.  So, using properties of logarithms
  and of exponents, we have
  \begin{align*}
    a^{b} &= \left( \sqrt{2} \right)^{\log_{2}(9)} \\
    &= \left( 2^{\nicefrac{1}{2}} \right)^{\log_{2}\left(3^{2}\right)}\\
    &= \left( 2^{\nicefrac{1}{2}} \right)^{2\log_{2}(3)}\\
    &= \left( 2 \right)^{\log_{2}(3)}\\
    &= 3.
  \end{align*}
  Because \(3\) is rational, we have shown that there exist irrational
  numbers \(a\) and \(b\) such that \(a^{b}\) is rational, as desired.
\end{proof}