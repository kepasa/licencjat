\emph{Programowanie półokreślone (SDP)} jest typem optymalizacji wypukłej będące uogólnieniem zadań programowania liniowego. Technika ta znajduje zastosowania w aproksymacji problemów trudnych obliczeniowo. Ta praca ma na celu wprowadzenie do teorii programowania półokreślonego, przedstawienie przykładów jej zastosowania do problemów MAX-CUT oraz MAX-2SAT przy użyciu biblioteki CSDP.

\subsection{Własności macierzy dodatnio półokreślonych}
Problemy SDP korzystają z własności macierzy dodatnio półokreślonych.

\begin{mydefn}
Niech \begin{math} S \in \mathbb{R}^{n\times n} \end{math} będzie symetryczną macierzą. Macierz S jest \emph{dodatnio półokreślona} wtedy i tylko wtedy, gdy dla każdego wektora $x \in \mathbb{R}^n$, $x^TSx \geqslant 0$.
%\begin{math}
%\Leftrightarrow \forall x \in \mathbb{R}^{n},\ x^{T}Sx \geqslant 0
%\end{math}
\end{mydefn}

\begin{pthm}
\label{sdproperties}
Następujące warunki są równoważne:
\begin{enumerate}[label={(\arabic*)}]
\item \begin{math}
\forall x \in \mathbb{R}^{n},\ x^{T}Sx \geqslant 0.
\end{math}
\item Wartości\ własne\ S\ są\ nieujemne.
\item \begin{math}
S=WDW^{T},\ W, D \in\mathbb{R}^{n\times n}\ oraz\ D\ jest\ diagonalna,\ d_{ij} \geqslant 0\end{math}.
\item \(S=Y^{T}Y\ dla\ pewnych\ macierzy\ Y \in \mathbb{R}^{n\times n} \).
\end{enumerate}
\end{pthm}

\begin{proof}\mbox{}
\begin{enumerate}
\setlength{\itemindent}{0.5in}
\item[$(1) \Rightarrow (2)$] Niech \(\lambda\) będzie wartością własną S, natomiast \(v \in \mathbb{R}^{n}\) odpowiadającym jej wektorem własnym. Oznacza to, że \(S\cdot v=\lambda\cdot v\). Mnożąc obie strony z lewej przez \(v^{T}\) otrzymujemy \(v^{T}\cdot S \cdot v = v^{T}\cdot \lambda \cdot v\). Z $(1)$ wynika, że \(0\leqslant \lambda \cdot v^{T} \cdot v\). Jako, że \(v^{T}\cdot v \geqslant 0\) otrzymujemy \(\lambda \geqslant 0\).
\item[$(2) \Rightarrow (3)$] Z symetryczności S, istnieje baza ortonormalna \(w_1\ldots w_n\) dla wartości własnych \(\lambda_1\ldots \lambda_n\) macierzy S. Korzystając z równości \(S\cdot w_i = \lambda_i \cdot w_i\), jeśli weźmiemy macierz \(W\), której kolumny stanowią wektory \(w_1\ldots w_n\) oraz macierz diagonalną \(D\) która na przekątnej ma kolejne wartości własne $\lambda_1, \ldots, \lambda_n$, otrzymamy \(S\cdot W = W\cdot D\). Jako, że macierz W jest ortonormalna, więc \(W\cdot W^{T} = I_n\). W konsekwencji \(S\cdot W \cdot W^{T} = S = W \cdot D \cdot W^{T}\). 
\item[$(3) \Rightarrow (4)$] Z założeń $(3)$, niech $D$ będzie macierzą, która ma na przekątnej nieujemne wartości własne. Stąd można ją rozłożyć na iloczyn \(D=Q\cdot Q^{T},\ Q \in\mathbb{R}^{n\times n}\), oraz Q jest diagonalna. Otzymujemy \(S = W\cdot Q\cdot Q^{T}\cdot W^{T}=W\cdot Q \cdot (W\cdot Q)^{T}\). Podstawiając \(Y = (W\cdot Q)^{T}\) otrzymujemy $(4)$.
\item[(\(4 \Rightarrow 1\))] Podstawiając \(S=Y^{T} Y\), dla wektora $x \in \mathbb{R}^n$ dostajemy \(x^{T}Y^{T}Yx = (Yx)^{T}(Yx) \geqslant 0\).
\end{enumerate}
\end{proof}

Przy pomocy dekompozycji $LDL^T$ w czasie wielomianowym można przedstawić macierz symetryczną $A$ w postaci \(A=LDL^{T}\), gdzie $L$ jest macierzą dolnotrójkątną, a $D$ jest macierzą diagonalną mającą na przekątnej wartości własne A. Macierz A jest dodatnio półokreślona wtedy i tylko wtedy, gdy jej wartości własne są nieujemne, tym samym w czasie wielomianowym można rozstrzygnąć czy dana macierz jest dodatnio półokreślona. Niech \(A \succeq 0\) oznacza, że A jest dodatnio półokreślona. 

\begin{prop}
\label{sdmatrixconvex}
Suma macierzy dodatnio półokreślonych jest dodatnio półokreślona. Podobnie, dowolna kombinacja wypukła macierzy dodatnio półokreślonych jest dodatnio półokreślona.
\end{prop}

\begin{proof}
Niech macierze \(A,B \in \mathbb{R}^{n\times n}\) będą dodatnio półokreślone. Pokażemy, że kombinacja wypukła $\lambda A + (1-\lambda)B,\ \lambda \in [0, 1]$ jest dodatnio półokreślona. Niech \(x\in \mathbb{R}^{n}\). Wtedy \(\ x^{T}[\lambda A + (1-\lambda)B]x=
\lambda x^{T}Ax + (1-\lambda)x^{T}Bx \geqslant 0\), bo korzystając z dodatniej półokreśloności A, B: \(x^{T}Ax \geqslant 0\ oraz\ x^{T}Bx \geqslant 0\).
\end{proof}

\begin{mydefn}
Dla macierzy symetrycznych \(A,B\in \mathbb{R}^{n\times n}\) \emph{iloczynem skalarnym Frobeniusa} nazywamy sumę \(\sum\limits_{i=1}^{n}\sum\limits_{j=1}^{n}A_{ij}B_{ij}\), który oznaczamy przez $A\bullet B$.
\end{mydefn}

\begin{remark} \label{obserwacja1}
Dla każdego wektora \(x\in \mathbb{R}^{n},\ (xx^{T})\) jest macierzą \(n\times n\) która jest dodatnio półokreślona.
\end{remark}
\begin{remark} \label{obserwacja2}
Dla każdego wektora \(x\in \mathbb{R}^n\): 
\begin{equation*}
x^{T}Ax = \sum\limits_{i=1}^{n}\sum\limits_{j=1}^{n}x_ix_jA_{ij}=\sum\limits_{i=1}^{n}\sum\limits_{j=1}^{n}(xx^{T})_{ij}A_{ij}=(xx^T)\bullet A.
\end{equation*}
\end{remark}

\begin{prop}
\label{prop2}
Dla macierzy symetrycznej \(A \in \mathbb{R}^{n\times n}\), następujące warunki są równoważne:
\begin{enumerate}[label={(\arabic*)}]
\item $A \succeq 0$.
\item Dla dowolnej macierzy $B \in \mathbb{R}^{n\times n},\ B \succeq 0$ zachodzi $A\bullet B \geqslant 0$.
\end{enumerate}
\end{prop}
\begin{proof} \mbox{}
\begin{enumerate}
\setlength{\itemindent}{0.5in}
\item[$(1) \Rightarrow (2)$] Niech $A \in \mathbb{R}^{n\times n}$ będzie macierzą dodatnio półokreśloną. Dla dowolnej macierzy dodatnio półokreślonej $B \in \mathbb{R}^{n\times n}$ istnieje rozkład w postaci $B=W^TW = \sum\limits_{1 \leq i \leq n} w_iw_i^T$, gdzie $w_i \in \mathbb{R}^n$ to i-ty wiersz macierzy W. Korzystając z obserwacji \ref{obserwacja2} otrzymujemy $A\bullet B = \sum\limits_{1 \leq i \leq n} [(w_iw_i^T)\bullet A] = \sum\limits_{1 \leq i \leq n} w_i^TAw_i \geq 0$.
\item[$(2) \Rightarrow (1)$] Przypuśćmy, że macierz $A \in \mathbb{R}^{n\times n}$ nie jest dodatnio półokreślona. Z twierdzenia \ref{sdproperties} wiemy, że $\exists a\in \mathbb{R}^{n}:\ a^TAa < 0$. Z obserwacji \ref{obserwacja1} oraz \ref{obserwacja2} wiemy, że $(aa^T)\bullet A = a^TAa < 0$ oraz $aa^T$ jest dodatnio półokreślona.
\end{enumerate}

%Przypuścmy, że macierz $A$ nie jest dodatnio półokreślona. Z %\Pthmref{sdproperties} wiemy, że $\exists a\in \mathbb{R}^{n}:\ a^TAa < 0$. Z %obserwacji wiemy, że $(aa^T)\bullet A = a^TAa < 0$ oraz $aa^T$ jest dodatnio półokreślona. Pozostaje przypadek, gdy $A \succeq 0$. Jako, że $B \succeq 0$ to wiemy, że istnieje rozkład $B = W^TW = \sum\limits_{1 \leq i \leq n} w_iw_i^T$, gdzie $w_i$ to wiersze $W$. Stąd $A\bullet B = \sum\limits_{1 \leq i \leq n} [(w_iw_i^T)\bullet A] = \sum\limits_{1 \leq i \leq n} w_i^TAw_i \geq 0$.
\end{proof}

\subsection{Technika programowania półokreślonego}
%\subsubsection{Wprowadzenie}

Niech \(M_n\) będzie stożkiem symetrycznych macierzy rzeczywistych \(n\times n\). Niech
\(A_1,A_2,\ldots,A_m,C \in M_n\) oraz niech \( b_1,b_2,\ldots,b_m \in \mathbb{R}\).
Zadanie \emph{programowania półokreślonego} jest dane jako:

\begin{alignat}{2}
&\text{zminimalizuj} \quad&& C\bullet X \nonumber \\[1ex]
&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& A_i\bullet X = b_i,\ i=1\ldots m  \label{eq:SDP} \\
& 	  \quad&& X \succeq 0,\ X \in M_n \nonumber
\end{alignat}


Jeśli macierze \(A_1,A_2,\ldots,A_m\) oraz \( C\) są diagonalne, to problem programowania półokreślonego staje się równoważny programowaniu liniowemu. \\
Dowolną macierz $\overline{X}\in \mathbb{R}^{n\times n}$ spełniającą warunki \eqref{eq:SDP} nazywamy \emph{rozwiązaniem dopuszczalnym}. Z propozycji \ref{sdmatrixconvex} wiemy, że kombinacja wypukła macierzy dodatnio półokreślonych jest wypukła. Stąd zbiór rozwiązań dopuszczalnych problemu \eqref{eq:SDP} jest wypukły.
%\\
%Aby otrzymać pełniejszy obraz programowania półokreślonego warto podać problem %dualny do problemu sformułowanego w.

%\begin{alignat}{2} \label{dualSDP}
%&\text{zmaksymalizuj} \quad&& b^{T}y \\[1ex]
%&\parbox{20ex}{z zachowaniem\\[0.1ex]warunków:} \quad&& \sum\limits_{i=1}^n %A_iy_i + S = C\ \nonumber \\
%& 	  \quad&& S \succeq 0,\ S \in M_n \nonumber
%\end{alignat}


%\subsubsection{Dualność}
%\begin{pthm} \textnormal{(Słaba dualność)}
%Jeśli $X$ będazie rozwiązaniem dopuszczalnym \ref{SDP} i $(y,S)$ będą %rozwiązaniami dopuszczalnymi \ref{dualSDP}, to \(C\bullet X \geqslant b^{T}y\).
%\end{pthm}
%\begin{proof}
%$X\ i\ (y,S)$ są rozwiązaniami dopuszczalnymi odpowiednio problemu prymalnego i  %dualnego. Otrzymujemy równanie \(C\bullet X = (\sum\limits_{i=1}^n A_iy_i + S)%\bullet X = \sum\limits_{i=1}^n y_i(A_i\bullet X) + S\bullet X = \sum%\limits_{i=1}^n b_iy_i + S\bullet X = b^{T}y + S\bullet X\). Z propozycji %\ref{prop2} \( S\bullet X \geqslant 0\) czyli \(C\bullet X \geqslant b^{T}y\).
%\end{proof}

%Tak jak w przypadku programowania liniowego przejście od problemu prymalego do %dualnego jest czysto syntaktyczne oraz obowiązuje słaba dualność. W %przeciwieństwie do LP silna dualność obowiązuje tylko pod pewnymi warunkami.
%\\
%\\
%Problem SDP posiada rozwiązanie \emph{silnie dopuszczalne}, gdy spełnia silnie %dodatnią półokreśloność, czyli macierz zmiennych jest dodatnio określona.
%\begin{pthm} \textnormal{(Silna dualność)}
%Niech zadania prymalne i dualne mają rozwiązanie dopuszczalne. Wtedy \%(opt_{primal} \geqslant opt_{dual}\), gdzie \(opt_{primal}\) i \(opt_{dual}\) %są rozwiązaniami optymalnymi. Ponadto jeśli rozwiązanie zadania prymalnego jest %dodatnio określone zachodzi
%\begin{enumerate}
%\item Zadanie dualne osiąga optimum (co nie zawsze zachodzi w przypadku SDP)
%\item \(opt_{primal} = opt_{dual}\)
%\end{enumerate}
%Symetryczne warunki zachodzą, gdy zadanie dualne posiada rozwiązanie dodatnio %określone.
%\end{pthm}

%\subsubsection{Przykład zastosowania}
%Prostym przykładem na zastosowanie SDP jest problem znalezienia maksymalnej wartości własnej macierzy symetrycznej \(A\in \mathbb{R}^{n\times n}\). Niech \(\lambda_1 \geqslant \lambda_2 \geqslant \ldots \geqslant \lambda_n\) będą wartościami własnymi A. Wtedy macierz \(tI_n - A\) ma wartości własne \(t-\lambda_1,t-\lambda_2,\ldots,t-\lambda_n\). Z twierdzenia \ref{sdproperties} wiemy, że  \(tI_n - A\) jest dodatnio półokreślona jeśli wszystkie wartości własne są nieujemne. Szukamy więc najmniejszego t takiego że \(t \geqslant \lambda_1\). Korzystając z postaci dualnej \ref{dualSDP}. \(t \in \mathbb{R},\ A_1=I_n,\ C=A,\ S=tI_n-A\). Ustawiając \(b=1\), uzyskujemy problem minimalizacyjny. Tak więc ostatecznie otrzymujemy instancję zadania półokreślonego \(\lambda_1 = min \lbrace t\ \vert\ tI_n-A \succeq 0 \rbrace \).
%\\
%\\
%\todo[inline]{dodać problem dualny}%