\tikzset{x=0.25cm,y=0.25cm}
\newsavebox{\el}
\sbox{\el}{
      \begin{tikzpicture}
      \draw[pattern=north east lines] (0,0) rectangle (1,1);
      \draw[pattern=north east lines] (1,0) rectangle (2,1);
      \draw[pattern=north east lines] (0,1) rectangle (1,2);
      \end{tikzpicture}
}

\newcommand{\El}{\usebox{\el}}

\subsection{A Non-Algebraic Induction Proof}
\begin{mydefn}
  An L-triomino is a geometric figure consisting of three congruent squares arranged into an L,
  \El. We say that a region \(R\) can be tiled by L-triominoes if the region can be completely
  covered by L-triominoes without overlapping.
\end{mydefn}
\begin{pthm}\pthmlabel{chessboard}
  For any natural number \(n\), any \(2^n\times2^n\) chessboard composed of squares of a particular
  size with one square removed can be
  completely tiled using L-triominoes consisting of squares of the same given size.
\end{pthm}
\begin{proof}
We will show that for any natural number \(n\), any chessboard of size
\(2^{n}\times2^{n}\) with one square deleted can always be tiled using
L-triominoes using mathematical induction.

\begin{basisstep}
  For our basis step, we need to show that any \(2^{1}\times2^{1}=2\times2\) chess
  board with one square deleted can be tiled using L-triominoes.

  We first note that we can always rotate the \(2\times2\) chessboard 
  so that the deleted square is in the lower left corner:
  {
    \begin{tikzpicture}
      \draw[line width=0.04,fill=black](2.0,2.0) rectangle (0.0,0.0);
      \draw[line width=0.04](4.0,4.0) rectangle (2.0,2.0);
      \draw[line width=0.04](2.0,4.0) rectangle (0.0,2.0);
      \draw[line width=0.04](4.0,2.0) rectangle (2.0,0.0);
    \end{tikzpicture} 
  }.  Here, it is obvious that we can exactly tile the three remaining squares of
  the \(2\times2\) chessboard with one L-triomino, {
    \begin{tikzpicture}
      \draw[line width=0.04,fill=black](2.0,2.0) rectangle (0.0,0.0);
      \draw[line width=0.04,pattern=north east lines](4.0,4.0) rectangle (2.0,2.0);
      \draw[line width=0.04,pattern=north east lines](2.0,4.0) rectangle (0.0,2.0);
      \draw[line width=0.04,pattern=north east lines](4.0,2.0) rectangle (2.0,0.0);
    \end{tikzpicture} 
  }.

\end{basisstep}
\begin{inductivestep}
  For our inductive step, we will fix \(k\) to be some positive integer and show that
  if every \(2^{k}\times2^{k}\) chessboard with one square deleted can be tiled
  using L-triominoes, then every \(2^{k+1}\times2^{k+1}\) chessboard with one square deleted can be tiled
  using L-triominoes.

  Given any \(2^{k+1}\times2^{k+1}\) chessboard with one square deleted, we first
  partition the chessboard into four regions of size \(2^{k}\times2^{k}\), and
  then rotate the chessboard so that the missing square is in the lower left
  \(2^{k}\times2^{k}\) region, as shown in \Figref{big}.
  \begin{figure}[ht]
    \begin{center}
      {
	\begin{tikzpicture}
	  \draw[line width=0.04](0.0,0.0) rectangle (16.0,16.0);
	  \draw[line width=0.04,fill=black](3,3) rectangle (3.5,3.5);
%
	  \draw[line width=0.20](8.0,0.0) -- (8.0,16.0);
	  \draw[line width=0.20](0.0,8.0) -- (16.0,8.0);
	\end{tikzpicture} 
      }
    \end{center}
    \caption{A \(2^{k+1}\times2^{k+1}\) chessboard partitioned into four
    \(2^{k}\times2^{k}\) regions.}\figlabel{big}
  \end{figure}

  It is clear that the lower left region is precisely a 
  \(2^{k}\times2^{k}\) chessboard with one square deleted
  and thus, by our inductive hypothesis, can be tiled with L-triominoes.

  Next, we place one L-triomino adjacent to the lower left region so that it
  occupies one square in each of the remaining three \(2^{k}\times2^{k}\) regions
  as shown in \Figref{bigWithTile}.

\begin{figure}[ht]
  \begin{center}
{
\begin{tikzpicture}
\draw[line width=0.04](0.0,0.0) rectangle (16.0,16.0);
\draw[line width=0.04,fill=black](3,3) rectangle (3.5,3.5);
\draw[line width=0.04,pattern=north east lines](8,8) rectangle (9,9);
\draw[line width=0.04,pattern=north east lines](7,8) rectangle (8,9);
\draw[line width=0.04,pattern=north east lines](8,7) rectangle (9,8);
%
\draw[line width=0.20](8.0,0.0) -- (8.0,16.0);
\draw[line width=0.20](0.0,8.0) -- (16.0,8.0);
\end{tikzpicture} 
}
\end{center}
\caption{A \(2^{k+1}\times2^{k+1}\) chessboard with one L-triomino.}\figlabel{bigWithTile}
\end{figure}

  Because the L-triomino covers exactly one tile in each of the three
  \(2^{k}\times2^{k}\) regions, the remaining squares form three
  \(2^{k}\times2^{k}\) chessboards each with one square deleted, and thus they
  can, by our inductive hypothesis, be tiled with L-triominoes as well, and thus
  the entire
  \(2^{k+1}\times2^{k+1}\) chessboard can be tiled with L-triominoes.  This
  completes the inductive step.

\end{inductivestep}
Since we have proven the basis step and the inductive step, we have proven our proposition, that for
any natural number \(n\), any \(2^n\times2^n\) chessboard with one square removed can be completely
tiled using L-triominoes.
\end{proof}