\babel@toc {polish}{}
\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}W\IeC {\l }asno\IeC {\'s}ci macierzy dodatnio p\IeC {\'o}\IeC {\l }okre\IeC {\'s}lonych}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Technika programowania p\IeC {\'o}\IeC {\l }okre\IeC {\'s}lonego}{4}{subsection.1.2}
\contentsline {section}{\numberline {2}Zastosowanie programowania p\IeC {\'o}\IeC {\l }okre\IeC {\'s}lonego w aproksymacji}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Problem MAX-CUT}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Reprezentacja w postaci \IeC {\'s}ci\IeC {\'s}le kwadratowej i wektorowej}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Reprezentacja w postaci zadania p\IeC {\'o}\IeC {\l }okre\IeC {\'s}lonego}{5}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Algorytm zaokr\IeC {\k a}glania randomizowanego}{7}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Generalizacja na grafy z ujemnymi wagami kraw\IeC {\k e}dzi}{9}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}MAX-2SAT}{10}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Reprezentacja w postaci \IeC {\'s}ci\IeC {\'s}le kwadratowej i wektorowej}{11}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Reprezentacja w postaci zadania p\IeC {\'o}\IeC {\l }okre\IeC {\'s}lonego}{11}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Algorytm zaokr\IeC {\k a}glania randomizowanego}{12}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Kolorowanie graf\IeC {\'o}w 3-kolorowalnych}{12}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Algorytm zaokr\IeC {\k a}glania randomizowanego}{14}{subsubsection.2.3.1}
\contentsline {section}{\numberline {3}Implementacja problemu MAX-CUT}{15}{section.3}
