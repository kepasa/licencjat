\subsection{Checking and Disproving Injectivity}
We start by defining the set \(D=\R-\left\{ 3 \right\}\), and looking at the function
\(f\coln{}D\to\R\) defined by
\begin{equation*}
  f\coln{}x\mapsto\frac{2x^{3}}{x-3}.
\end{equation*}
We will investigate the question of whether \(f\) is injective or not.

We will check by letting \(x\) and \(z\) be arbitrary elements of the domain,
\(D=\R-\left\{ 3 \right\}\), of
\(f\).  If we can show that the \emph{only} way for this to happen is for
\(x=z\), then we know that \(f\) is injective.  On the other hand, if we can
find \(x\neq{}z\) so that \(f(x)=f(z)\), then we know that \(f\) is \emph{not}
injective.

We assume that \(f(x)=f(z)\). So,
\begin{equation}
  \frac{2x^{3}}{x-3}=\frac{2z^{3}}{z-3}.
  \eqlabel{eq1}
\end{equation}

We can multiply \eqref{eq1} by
\(\left(x-3\right)\left(z-3\right)\) and simplify to get
\begin{align}
  \frac{\cancel{2}x^{3}}{\cancel{(x-3)}}\cancel{(x-3)}(z-3)&=\frac{\cancel{2}z^{3}}{\cancel{(z-3)}}(x-3)\cancel{(z-3)}\notag\\
  &\text{ or }\notag\\
  {x^{3}}(z-3)&={z^{3}}(x-3).
  \eqlabel{eq2}
\end{align}
Next we subtract \(z^{3}(x-3)\) to obtain
\begin{align}
  {x^{3}}(z-3)-{z^{3}}(x-3)&=0.
  \eqlabel{eq3}
\end{align}
By expanding, collecting like terms, and factoring \eqref{eq3}, we have
\begin{align}
 0&={x^{3}}(z-3)-{z^{3}}(x-3)     \notag\\
  &= x^{3}z-3x^{3}-x\,z^{3}+3z^{3}\notag\\
  &=x^{3}z-x\,z^{3}-3x^{3}+3z^{3} \notag\\
  &=x^{3}z-x\,z^{3}-3(x^{3}-z^{3})\notag\\
  &=xz(x^{2}-z^{2})-3(x^{3}-z^{3})\notag\\
  &=xz(x+z)(x-z)-3(x^{2}+x\,z+z^{2})(x-z) \notag\\
  &=\left(xz(x+z)-3(x^{2}+x\,z+z^{2})\right)(x-z).
  \eqlabel{eq4}
\end{align}
From \eqref{eq4}, we can see that if \(f(x)=f(z)\), then either
\begin{gather}
  x-z=0\text{ and thus }x=z\notag\\
  \text{or}\notag\\
  xz(x+z)-3(x^{2}+x\,z+z^{2})=0.  \eqlabel{fact1}
\end{gather}

If we can show that for all \(x,z\in{}D\), 
\(xz(x+z)-3(x^{2}+x\,z+z^{2})\neq0\),
then we will
have shown that \(f\) is injective.  

Otherwise, if we can find \(x\neq{}z\) so that
\(xz(x+z)-3(x^{2}+x\,z+z^{2})=0\), we have a likely candidate to show that \(f\)
is \emph{not} injective.

Now, if \(x=0\), then \(f(x)=f(0)=\frac{2\cdot0^3}{0-3}=0\), and thus if
\(f(x)=f(z)\), then 
\(f(z)=\frac{2z^{3}}{z-3}=0\).
Multiplying both sides by \(z-3\) gives us \(z=0\).  So, of \(f(0)=f(z)\), then \(z=0\).

Having dealt with the case \(x=0\) separately, we can now assume that
\(x\neq0\) and, by symmetry,  \(z\neq0\).  

Since \(x\neq 0\), we can define \(a=\nicefrac{z}{x}\), so that \(z=a\,x\), and substitute \(z=a\,x\) in
\eqref{fact1}. Expanding and then factoring a common factor of \(x^{2}\), we get
\begin{align}
  0&=xz(x+z)-3(x^{2}+x\,z+z^{2})\notag\\
   &=x(a\,x)(x+a\,x)-3x^{2}-3x\,(a\,x)-3(a\,x)^{2}\notag\\
   &=x(a\,x)(x+a\,x)-3(x^{2}+x\,(a\,x)+(a\,x)^{2})\notag\\
   &=a\,x^{3}+a^{2}x^{3}-3(x^{2}+a\,x^{2}+a^{2}x^{2})\notag\\
   &=(a^{2}+a)x^{3}-3(a^{2}+a+1)x^{2}\notag\\
   &=x^{2}\left( (a^{2}+a)x-3(a^{2}+a+1)\right).\eqlabel{fact2}
\end{align}
We have already said that \(x\neq0\), so \eqref{fact2} implies that 
\begin{equation*}
  (a^{2}+a)x-3(a^{2}+a+1)=0.
\end{equation*}
and thus
\begin{equation}
  (a^{2}+a)x=3(a^{2}+a+1).
  \eqlabel{lin1}
\end{equation}

Recalling that \(a=\nicefrac{z}{x}\) and we want to find \(x\neq z\) so that
\(f(x)=f(z)\), any value of \(a\) other than \(a=1\) is reasonable.  In
particular, as long as we choose \(a\) so that \(a\neq0\) and \(a\neq-1\) we can
divide by \(a^{2}+a\) in \eqref{lin1} getting
\begin{equation}
  x=\frac{3(a^{2}+a+1)}{a^{2}+a}.
  \eqlabel{lin2}
\end{equation}
Now let's pick a value of \(a\).  If we try \(a=-2\), we get
\begin{align}
  x&=\frac{3\big((-2)^{2}+(-2)+1\big)}{(-2)^{2}+(-2)}\notag\\
   &=\frac{3(4-2+1)}{4-2}\notag\\
   &=\frac{3(3)}{2}\notag\\
   &=\frac{9}{2},
  \eqlabel{lin3}
\end{align}
and \(z=a\,x=(-2)(\nicefrac{9}{2})=-9\).

Clearly \(-9\neq\nicefrac{9}{2}\), so if \(f(-9)=f(\nicefrac{9}{2})\), then we
will have shown that \(f\) is not injective.  Checking, we see that
\newcommand*{\nh}{\ensuremath{\left(\nicefrac{9}{2}\right)}}
\begin{align*}
  f(-9)&=\frac{2\cdot(-9)^{3}}{-9-3} &&& f\left(\nicefrac{9}{2}\right)&=\frac{2\cdot\nh^{3}}{\nh-3}\\
       &=\frac{-2\cdot9^3}{-12}      &&&                              &=\frac{{9^3}/{2^2}}{(9-6)/{2}}\\
       &=\frac{9^3}{6}               &&\text{and}&                              &=\frac{9^3/2}{3}\\
       &=\frac{243}{2}               &&&                              &=\frac{9^3}{6}\\
       &                             &&&                              &=\frac{243}{2},
\end{align*}
and thus we have that \(-9\neq\nicefrac{9}{2}\) and \(f\left( -9 \right)=f\left(
\nicefrac{9}{2} \right)\), so \(f\) is not injective. We are now ready to state a conjecture and
proof.

\begin{pthm}\pthmlabel{injection}
  Let \(D=\R-\left\{ 3 \right\}\), and let the function
  \(f\coln{}D\to\R\) be defined by
  \begin{equation*}
    f\coln{}x\mapsto\frac{2x^{3}}{x-3}.
  \end{equation*}
  The function \(f\) is not injective.
\end{pthm}
\begin{proof}
  We will show that the function \(f(x)=\frac{2x^{3}}{x-3}\) is not an injection by showing two
  distinct elements of the domain that map to the same element of the codomain. That is, we will
  find \(x_{1}, x_{2}\in D\) with \(x_{1}\neq x_{2}\) and \(f(x_{1})=f(x_{2})\).

  Let \(x_{1}=-9\) and let \(x_{2}=\nicefrac{9}{2}\). Since \(x_{1}\neq 3\) and \(x_{2}\neq 3\), we
  have that \(x_{1}\in D\) and \(x_{2}\in D\). It is also clear that \(x_{1}\neq x_{2}\). Thus, it
  remains to show that \(f(x_{1})=f(x_{2})\).

By direct calculation, we see that
\begin{align*}
  f(x_{1})&=f(-9)                    &&& f(x_{2})  &=f\left(\nicefrac{9}{2}\right)\\
       &=\frac{2\cdot(-9)^{3}}{-9-3} &&&           &=\frac{2\cdot\nh^{3}}{\nh-3}\\
       &=\frac{-2\cdot9^3}{-12}      &&&           &=\frac{{9^3}/{2^2}}{(9-6)/{2}}\\
       &=\frac{9^3}{6}               &&\text{and}& &=\frac{9^3/2}{3}\\
       &=\frac{243}{2}               &&&           &=\frac{9^3}{6}\\
       &                             &&&           &=\frac{243}{2},
\end{align*}
and thus \(f(x_{1})=f(x_{2})\).

Since we have shown that \(-9\neq\nicefrac{9}{2}\) and \(f\left( -9 \right)=f\left(
\nicefrac{9}{2} \right)\), we have shown that \(f\) is not injective.
\end{proof}